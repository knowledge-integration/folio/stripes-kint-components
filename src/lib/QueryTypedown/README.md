# QueryTypedown Component

The `QueryTypedown` component is a specialized version of the `Typedown` component that fetches its dropdown options based on a query. It uses a `pathMutator` function to construct the API path based on user input and then fetches the data using the `useTypedownData` hook.  This makes it easy to create typeaheads that query a backend API.

## Basic Usage

```javascript
import QueryTypedown from '@k-int/stripes-kint-components';

<QueryTypedown
  label="Search for something"
  path="your/api/endpoint"
  pathMutator={(query, basePath) => query ? `${basePath}?search=${query}` : basePath}
  input={{
    name: 'mySearchField',
    value: '',
    onChange: (value) => console.log(value),
  }}
/>
```

## Props

| Name | Type | Description                                                                                                                                                                                                                         | Default | Required |
|---|---|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|---|
| dataFormatter | func | A function `(data)` that formats the data returned by the API before it's passed to the underlying `Typedown` component. This is useful if the API response structure doesn't directly match the expected format for `dataOptions`. | `d => d` (returns data as-is) | ✕ |
| path | string | The base path for the API call.                                                                                                                                                                                                     |  | ✓ |
| pathMutator | func | A function `(query, basePath)` that takes the user's query as typed into the component and the base path and returns the full API path to be called. This allows for dynamic path construction based on user input.                 |  | ✓ |
|...rest |  | All other props are passed directly to the underlying `Typedown` component.  See the `Typedown` documentation for a full list of available props.                                                                                   |  | ✕ |

## How it works

1.  The `QueryTypedown` component receives a `path` (base API path) and a `pathMutator` function.
2.  It maintains an internal state `userQuery` to store the current value typed by the user.
3.  The `onType` prop is overridden to update the `userQuery` state.
4.  An effect uses the `pathMutator` function to generate a `callPath` based on the `userQuery` and `path`.
5.  The `useTypedownData` hook fetches data from the generated `callPath`.
6.  The fetched data is passed through the `dataFormatter` (if provided) to transform the data into a format suitable for the `Typedown` component's `dataOptions` prop.
7.  Finally, the `QueryTypedown` component renders the `Typedown` component, passing along the formatted data and all other props.

## Example: Custom Data Formatting

Let's say your API returns data in the following format:

```json
{
  "results": [
    { "idBySomeOtherName": 1, "label": "Option 1" },
    { "idBySomeOtherName": 2, "label": "Option 2" }
  ]
}
```

You can use the `dataFormatter` prop to extract the relevant data:

```javascript
<QueryTypedown
  label="Search for something"
  path="your/api/endpoint"
  pathMutator={(query, basePath) => query? `${basePath}?search=${query}`: basePath}
  dataFormatter={data => data?.results || []} // Extract the 'results' array
  input={{
    name: 'mySearchField',
    value: '',
    onChange: (value) => console.log(value),
  }}
  uniqueIdentificationPath="idBySomeOtherName" // Important: Tell Typedown which property is the ID
  filterPath="label" // And which to display/filter on
/>
```

This example extracts the `results` array from the API response and passes it to the `Typedown` component.  It also sets the `uniqueIdentificationPath` and the `filterPath` props for the underlying `Typedown` component.  This is crucial to ensuring the `Typedown` works as expected with the formatted data.
