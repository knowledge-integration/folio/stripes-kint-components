import React from 'react';

import { waitFor } from '@folio/jest-config-stripes/testing-library/react';

import { Button, TestForm } from '@folio/stripes-erm-testing';
import { Field } from 'react-final-form';
import SettingField from './SettingField';

import { renderWithKintHarness } from '../../../../test/jest/helpers';

const onSubmit = jest.fn();
const onSave = jest.fn().mockResolvedValue();

jest.mock('./EditSettingValue', () => () => <div>EditSettingValue</div>);
jest.mock('./RenderSettingValue', () => () => <div>RenderSettingValue</div>);

jest.mock('../../hooks');


const setting = {
  id: 'ff8081817d94374a017d94449a660049',
  key: 'S3BucketName',
  section: 'fileStorage',
  settingType: 'String',
  value: 'diku-shared'
};

describe('SettingField', () => {
  let renderComponent;
  beforeEach(async () => {
    renderComponent = renderWithKintHarness(
      <TestForm
        initialValues={{}}
        onSubmit={onSubmit}
      >
        <Field
          component={SettingField}
          name="test"
          onSave={onSave}
          settingData={{
            currentSetting: setting
          }}
        />
      </TestForm>
    );
  });

  it('renders RenderSettingValue', async () => {
    const { getByText } = renderComponent;
    await waitFor(async () => expect(await getByText('RenderSettingValue')).toBeInTheDocument());
  });

  test('renders the edit button', () => {
    Button('Edit').exists();
  });

  it('clicking edit/save works as expected', async () => {
    const { findByText } = renderComponent;
    // before clicking on edit button, we should be rendering setting value
    await waitFor(async () => expect(await findByText('RenderSettingValue')).toBeInTheDocument());

    // Clicking edit button
    await waitFor(async () => { await Button('Edit').click(); });

    // Should be rendering the "edit" for a setting value
    await waitFor(async () => expect(await findByText('EditSettingValue')).toBeInTheDocument());

    // Clicking save button
    await waitFor(async () => { await Button('Save').click(); });

    // Should be rendering the setting value again
    await waitFor(async () => expect(await findByText('RenderSettingValue')).toBeInTheDocument());
  });
});
