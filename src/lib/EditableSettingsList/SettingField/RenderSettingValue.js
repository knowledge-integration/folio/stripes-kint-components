import PropTypes from 'prop-types';

import { useKintIntl } from '../../hooks';

const RenderSettingValue = (props) => {
  const {
    currentSetting: setting,
    intlKey: passedIntlKey,
    intlNS: passedIntlNS,
    labelOverrides = {},
    refdata, // TODO ... can we get away without passing these in now, using useRefdata?
    templates // TODO ... can we get away without passing these in now, using useTemplates?
  } = props;

  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const NoCurrentValue = kintIntl.formatKintMessage({
    id: 'settings.noCurrentValue',
    overrideValue: labelOverrides?.noCurrentValue
  });

  const defaultText = `[${kintIntl.formatKintMessage({ id: 'settings.default', overrideValue: labelOverrides?.default })}]`;
  switch (setting.settingType) {
    case 'Refdata': {
      if (setting.value) {
        return refdata?.filter((obj) => obj.value === setting.value)[0]?.label ?? null;
      }

      if (setting.defValue) {
        const defValue = refdata?.filter((obj) => obj.value === setting.defValue)[0]?.label;
        return `${defaultText} ${defValue}`;
      }

      return NoCurrentValue;
    }
    case 'Password': {
      if (setting.value) {
        return '********';
      }

      if (setting.defValue) {
        return `${defaultText} ********`;
      }

      return NoCurrentValue;
    }
    case 'Template': {
      const templateValue = templates.filter((obj) => {
        const settingId = setting.value || setting.defValue;
        return obj.id === settingId;
      })[0];
      return templateValue?.name || NoCurrentValue;
    }
    default: {
      if (setting.value) {
        return setting.value;
      }

      if (setting.defValue) {
        return `${defaultText} ${setting.defValue}`;
      }
      return NoCurrentValue;
    }
  }
};

RenderSettingValue.propTypes = {
  currentSetting: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  refdata: PropTypes.arrayOf(PropTypes.object),
  templates: PropTypes.arrayOf(PropTypes.object),
};

export default RenderSettingValue;
