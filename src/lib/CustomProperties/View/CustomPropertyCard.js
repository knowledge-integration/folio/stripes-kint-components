import PropTypes from 'prop-types';
import { FormattedNumber } from 'react-intl';

import {
  Card,
  Col,
  FormattedUTCDate,
  InfoPopover,
  KeyValue,
  List,
  NoValue,
  Row,
} from '@folio/stripes/components';

import * as CUSTPROP_TYPES from '../../constants/customProperties';
import { useKintIntl } from '../../hooks';

const CustomPropertyCard = ({
  ctx,
  customProperty,
  customPropertyDefinition = {},
  index,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {}
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  // We only need to display primary and set properties
  const toDisplay = !!(customPropertyDefinition.primary || customProperty);

  if (!toDisplay) {
    return null;
  }

  const internalFalse = customProperty?.internal === false ||
    (customProperty?.internal === undefined && customPropertyDefinition.defaultInternal === false);

  const renderValue = () => {
    if (!customProperty) {
      return (
        kintIntl.formatKintMessage({
          id: 'notSet',
          overrideValue: labelOverrides.notSet
        })
      );
    }

    switch (customPropertyDefinition.type) {
      case CUSTPROP_TYPES.MULTI_REFDATA_CLASS_NAME:
        return (
          <List
            items={customProperty.value?.map((item) => item?.label ?? item?.value)}
            listStyle="bullets"
          />
        );
      case CUSTPROP_TYPES.REFDATA_CLASS_NAME:
        return customProperty.value?.label ?? customProperty.value?.value;
      case CUSTPROP_TYPES.DECIMAL_CLASS_NAME:
      case CUSTPROP_TYPES.INTEGER_CLASS_NAME:
        return <FormattedNumber value={customProperty.value} />;
      case CUSTPROP_TYPES.DATE_CLASS_NAME:
        return <FormattedUTCDate value={customProperty.value} />;
      case CUSTPROP_TYPES.TEXT_CLASS_NAME:
      default:
        return customProperty.value;
    }
  };

  return (
    <Card
      key={`customPropertyCard-${ctx}[${index}]`}
      headerStart={
        <>
          <strong>
            {customPropertyDefinition.retired ?
              (kintIntl.formatKintMessage({
                id: 'customProperty.retiredName',
                overrideValue: labelOverrides.retiredName
              }, { name: customPropertyDefinition.label })) :
              customPropertyDefinition.label
            }
          </strong>
          {customPropertyDefinition.description ? (
            <InfoPopover
              content={customPropertyDefinition.description}
            />
          ) : null}
        </>
      }
      id={`customproperty-card-${index}`}
    >
      <Row>
        <Col xs={6}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'valueOrValues',
                overrideValue: labelOverrides.valueOrValues
              })
            }
          >
            {renderValue()}
          </KeyValue>
        </Col>
        {customProperty?.note ? (
          <Col xs={6}>
            <KeyValue
              label={
                kintIntl.formatKintMessage({
                  id: 'customProperties.internalNote',
                  overrideValue: labelOverrides.internalNote
                })
              }
            >
              <span
                style={{ whiteSpace: 'pre-wrap' }}
              >
                {customProperty.note}
              </span>
            </KeyValue>
          </Col>
        ) : null}
      </Row>
      <Row>
        <Col xs={6}>
          <KeyValue
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.visibility',
                overrideValue: labelOverrides.visibility
              })
            }
          >
            {kintIntl.formatKintMessage({
              id: internalFalse ? 'customProperties.internalFalse' : 'customProperties.internalTrue',
              overrideValue: internalFalse ? labelOverrides.internalFalse : labelOverrides.internalTrue,
            })}
          </KeyValue>
        </Col>
        {internalFalse &&
          <Col xs={6}>
            <KeyValue
              label={
                kintIntl.formatKintMessage({
                  id: 'customProperties.publicNote',
                  overrideValue: labelOverrides.publicNote
                })
              }
            >
              {customProperty?.publicNote ? (
                <span
                  style={{ whiteSpace: 'pre-wrap' }}
                >
                  {customProperty.publicNote}
                </span>
              ) : (
                <NoValue />
              )}
            </KeyValue>
          </Col>
        }
      </Row>
    </Card>
  );
};

CustomPropertyCard.propTypes = {
  ctx: PropTypes.string,
  customProperty: PropTypes.object,
  customPropertyDefinition: PropTypes.object,
  index: PropTypes.number,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
};

export default CustomPropertyCard;
