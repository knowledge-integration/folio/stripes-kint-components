import React from 'react';

import { MemoryRouter } from 'react-router-dom';
import { waitFor } from '@folio/jest-config-stripes/testing-library/react';

import { data } from './testResources';
import CustomPropertiesViewCtx from './CustomPropertiesViewCtx';
import { renderWithKintHarness } from '../../../../test/jest';

jest.mock('../../hooks');
jest.mock('./CustomPropertyCard', () => () => <div>CustomPropertyCard</div>);

describe('CustomPropertiesViewCtx', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesViewCtx
          {...data}
        />
      </MemoryRouter>
    );
  });

  it('renders CustomPropertyCard component ', () => {
    const { getByText } = renderComponent;
    waitFor(() => expect(getByText('CustomPropertyCard')).toBeInTheDocument());
  });
});
