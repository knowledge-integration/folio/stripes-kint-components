const contexts = [
  'isNull',
  'OpenAccess'
];

const customProperties = {
  'AuthorIdentification': [{
    'id': 2,
    'publicNote': 'public note',
    'note': 'internal note',
    'internal': true,
    'value': {
      'id': '2c91809c813e654501813e6c3bc50061',
      'value': 'orcid',
      'label': 'ORCID'
    },
    'type': {
      'id': '2c91809c813e654501813e6c3c6a0066',
      'retired': false,
      'ctx': 'OpenAccess',
      'name': 'AuthorIdentification',
      'primary': true,
      'category': {
        'id': '2c91809c813e654501813e6c3bbb005e',
        'desc': 'AuthIdent',
        'internal': false,
        'values': [{
            'id': '2c91809c813e654501813e6c3bc50061',
            'value': 'orcid',
            'label': 'ORCID'
          },
          {
            'id': '2c91809c813e654501813e6c3bcf0064',
            'value': 'ringgold_id',
            'label': 'Ringgold ID'
          },
          {
            'id': '2c91809c813e654501813e6c3bcc0063',
            'value': 'over_ip_range',
            'label': 'Over IP Range'
          },
          {
            'id': '2c91809c813e654501813e6c3bd20065',
            'value': 'ror_id',
            'label': 'ROR ID'
          },
          {
            'id': '2c91809c813e654501813e6c3bbd005f',
            'value': 'other',
            'label': 'Other'
          },
          {
            'id': '2c91809c813e654501813e6c3bc90062',
            'value': 'over_institute',
            'label': 'Over Institute'
          },
          {
            'id': '2c91809c813e654501813e6c3bc20060',
            'value': 'email_domain',
            'label': 'Email Domain'
          }
        ]
      },
      'defaultInternal': true,
      'label': 'Author Identification',
      'description': 'Author Identification',
      'weight': 0,
      'type': 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdata'
    }
  }]
};


const labelOverrides = {
  'defaultTitle': 'ƒ defaultTitle() {}',
  'noContext': '<Memo />',
  'OpenAccess': '<Memo />',
  'retiredName': 'ƒ retiredName() {}'
};

const data = {
  'ctx': 'isNull',
  'customProperties': {
    'AuthorIdentification': [{
      'id': 2,
      'internal': true,
      'value': {
        'id': '2c91809c815d4e6d01815d55adfd0060',
        'value': 'email_domain',
        'label': 'Email Domain'
      },
      'type': {
        'id': '2c91809c815d4e6d01815d55af070066',
        'retired': false,
        'ctx': 'OpenAccess',
        'name': 'AuthorIdentification',
        'primary': true,
        'category': {
          'id': '2c91809c815d4e6d01815d55adf7005e',
          'desc': 'AuthIdent',
          'internal': false,
          'values': [{
              'id': '2c91809c815d4e6d01815d55adf9005f',
              'value': 'other',
              'label': 'Other'
            },
            {
              'id': '2c91809c815d4e6d01815d55ae080063',
              'value': 'over_ip_range',
              'label': 'Over IP Range'
            },
            {
              'id': '2c91809c815d4e6d01815d55ae050062',
              'value': 'over_institute',
              'label': 'Over Institute'
            },
            {
              'id': '2c91809c815d4e6d01815d55ae010061',
              'value': 'orcid',
              'label': 'ORCID'
            },
            {
              'id': '2c91809c815d4e6d01815d55ae0c0064',
              'value': 'ringgold_id',
              'label': 'Ringgold ID'
            },
            {
              'id': '2c91809c815d4e6d01815d55ae0f0065',
              'value': 'ror_id',
              'label': 'ROR ID'
            },
            {
              'id': '2c91809c815d4e6d01815d55adfd0060',
              'value': 'email_domain',
              'label': 'Email Domain'
            }
          ]
        },
        'defaultInternal': true,
        'label': 'Author Identification',
        'description': 'Author Identification',
        'weight': 0,
        'type': 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdata'
      }
    }]
  },
  'customPropertiesEndpoint': 'erm/custprops',
  'id': 'supplementaryProperties',
  'labelOverrides': {
    'defaultTitle': 'ƒ defaultTitle() {}',
    'noContext': '<Memo />',
    'OpenAccess': '<Memo />',
    'retiredName': 'ƒ retiredName() {}'
  }
};

export {
  contexts,
  customProperties,
  labelOverrides,
  data
};
