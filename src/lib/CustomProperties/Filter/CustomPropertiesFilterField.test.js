import React from 'react';
import { FieldArray } from 'react-final-form-arrays';


import { TestForm } from '@folio/stripes-erm-testing';
import { MemoryRouter } from 'react-router-dom';

import CustomPropertiesFilterField from './CustomPropertiesFilterField';
import customProperties from '../../../../test/jest/customProperties';
import { renderWithKintHarness } from '../../../../test/jest';
import { data } from './testResources';

jest.mock('../../hooks');
const onSubmit = jest.fn();

let renderComponent;
describe('CustomPropertiesFilterField', () => {
  renderComponent = renderWithKintHarness(
    <MemoryRouter>
      <TestForm
        initialValues={{
          filters: data.filters
        }}
        onSubmit={onSubmit}
      >
        <FieldArray name="filters">
          {({ fields }) => fields.map((name, index) => (
            <CustomPropertiesFilterField
              key={`custom-property-filter-field-${name}[${index}]`}
              customProperties={customProperties}
              fields={fields}
              index={index}
              name={name}
            />
          ))}
        </FieldArray>
      </TestForm>
    </MemoryRouter>
  );

  test('displays the Custom property label', () => {
    const { getByTestId } = renderComponent;
    expect(getByTestId('selected-custprop-name-0')).toHaveTextContent('Author Identification');
  });
});
