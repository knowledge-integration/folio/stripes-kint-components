import { useState } from 'react';
import PropTypes from 'prop-types';

import { Col, MultiColumnList, Row, Spinner, Select, Button } from '@folio/stripes/components';

import { Form } from 'react-final-form';
import SearchField from '../../SearchField';
import { useCustomProperties, useKintIntl } from '../../hooks';

import css from '../../../../styles/CustomProperties.css';

const CustomPropertiesLookup = ({
  contextFilterOptions, // expects an array of the form [{value: "OpenAccess", label: "Open Access"}, {value: false, label: "None"}]
  customPropertiesEndpoint: endpoint,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  mclProps,
  onSelectCustomProperty,
  queryParams
}) => {
  const [nsValues, setNsValues] = useState({
    sort: 'label'
  });
  const [selectedContext, setSelectedContext] = useState();

  const { data: custprops, isLoading } = useCustomProperties({
    endpoint,
    nsValues,
    queryParams,
    returnQueryObject: true,
    ctx: selectedContext,
    options: {
      sort: [
        {
          path: 'ctx'
        },
        {
          path: 'weight'
        }
      ]
    }
  });

  const handleSearch = (query) => {
    setNsValues({
      ...nsValues,
      query,
    });
  };
  const [searchTerm, setSearchTerm] = useState('');

  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <>
      <Form
        enableReinitialize
        keepDirtyOnReinitialize
        onSubmit={() => handleSearch(searchTerm)}
      >
        {({ handleSubmit }) => (
          <form
            className={css.lookupSearchContainer}
            onSubmit={handleSubmit}
          >
            <SearchField
              ariaLabel={
                kintIntl.formatKintMessage({
                  id: 'customProperties.config.searchAriaLabel',
                  overrideValue: labelOverrides.searchAriaLabel,
                  fallbackMessage: 'custom-property-search-field'
                })
              }
              className={css.lookupSearch}
              marginBottom0
              onChange={e => setSearchTerm(e.target.value)}
              value={searchTerm}
            />
            <Button
              buttonClass={css.lookupSearchButton}
              buttonStyle="primary"
              marginBottom0
              type="submit"
            >
              {kintIntl.formatKintMessage({
                id: 'search',
                overrideValue: labelOverrides.search
              })}
            </Button>
          </form>
        )}
      </Form>
      <Row>
        <Col xs={6}>
          <Select
            dataOptions={contextFilterOptions}
            label={
              kintIntl.formatKintMessage({
                id: 'customProperties.ctx',
                overrideValue: labelOverrides.ctx
              })
            }
            onChange={(e) => {
              setSelectedContext(e.target.value);
            }}
            value={selectedContext}
          />
        </Col>
      </Row>
      {isLoading ?
        <Spinner /> :
        <MultiColumnList
          columnMapping={{
            'label': kintIntl.formatKintMessage({
              id: 'customProperties.label',
              overrideValue: labelOverrides.label
            }),
            'primary': kintIntl.formatKintMessage({
              id: 'customProperties.primary',
              overrideValue: labelOverrides.primary
            }),
            'ctx': kintIntl.formatKintMessage({
              id: 'customProperties.ctx',
              overrideValue: labelOverrides.ctx
            }),
            'weight': kintIntl.formatKintMessage({
              id: 'customProperties.weight',
              overrideValue: labelOverrides.weight
            }),
            'type': kintIntl.formatKintMessage({
              id: 'customProperties.type',
              overrideValue: labelOverrides.type
            }),
            'category': kintIntl.formatKintMessage({
              id: 'customProperties.category',
              overrideValue: labelOverrides.category
            }),
          }}
          contentData={custprops}
          formatter={{
            primary: data => {
              if (data?.primary) {
                return (
                  kintIntl.formatKintMessage({
                    id: 'yes',
                    overrideValue: labelOverrides.yes
                  })
                );
              } else {
                return (
                  kintIntl.formatKintMessage({
                    id: 'no',
                    overrideValue: labelOverrides.no
                  })
                );
              }
            },
            type: data => (
              kintIntl.formatKintMessage({
                id: `customProperties.type.${data?.type}`,
                overrideValue: labelOverrides?.[data?.type]
              })
            ),
            category: data => data?.category?.desc
          }}
          onRowClick={onSelectCustomProperty}
          visibleColumns={['label', 'primary', 'ctx', 'weight', 'type', 'category']}
          {...mclProps}
        />
      }
    </>
  );
};

CustomPropertiesLookup.propTypes = {
  contextFilterOptions: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string,
    label: PropTypes.oneOfType([
      PropTypes.element,
      PropTypes.string
    ])
  })),
  customPropertiesEndpoint: PropTypes.string,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  mclProps: PropTypes.object,
  onSelectCustomProperty: PropTypes.func,
  queryParams: PropTypes.object,
  refdataEndpoint: PropTypes.string,
};

export default CustomPropertiesLookup;
