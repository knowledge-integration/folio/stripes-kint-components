import React from 'react';

import { MemoryRouter } from 'react-router-dom';

import CustomPropertiesLookup from './CustomPropertiesLookup';
import renderWithKintHarness from '../../../../test/jest/helpers/renderWithKintHarness';

jest.mock('../../hooks');
jest.mock('../../SearchField', () => () => <div>SearchField</div>);

describe('CustomPropertiesLookup', () => {
  let renderComponent;
  beforeEach(() => {
    renderComponent = renderWithKintHarness(
      <MemoryRouter>
        <CustomPropertiesLookup
          contextFilterOptions={[
            {
              'value': '',
              'label': 'All'
            },
            {
              'value': 'isNull',
              'label': 'None'
            }
          ]}
          customPropertiesEndpoint="erm/custprops"
          onSelectCustomProperty={() => ({})}
          visibleColumns={['label', 'primary', 'ctx', 'weight', 'type', 'category']}
        />
      </MemoryRouter>
    );
  });

  it('renders expected options', () => {
    const { getByText } = renderComponent;
    expect(getByText('All')).toBeInTheDocument();
    expect(getByText('None')).toBeInTheDocument();
  });

  it('renders SearchField component ', () => {
    const { getByText } = renderComponent;
    expect(getByText('SearchField')).toBeInTheDocument();
  });
});
