import { useState } from 'react';
import PropTypes from 'prop-types';
import { Field, useFormState, useForm } from 'react-final-form';

import {
  Col,
  InfoPopover,
  MultiSelection,
  Row,
  Select,
  TextArea,
  TextField
} from '@folio/stripes/components';
import { required as requiredValidator, composeValidators } from '../../validators';

import {
  DECIMAL_CLASS_NAME,
  INTEGER_CLASS_NAME,
  TEXT_CLASS_NAME,
  REFDATA_CLASS_NAME,
  MULTI_REFDATA_CLASS_NAME,
  DATE_CLASS_NAME
} from '../../constants/customProperties';
import { useKintIntl } from '../../hooks';
import NumberField from '../../NumberField';

const CustomPropertyForm = ({
  contextFilterOptions,
  helpPopovers,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  refdata
}) => {
  const { values } = useFormState();
  const { change } = useForm();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const booleanToString = booleanValue => booleanValue?.toString();
  const stringToBoolean = stringValue => stringValue === 'true';

  const [contextOptions, setContextOptions] = useState(contextFilterOptions?.filter(ctx => ctx.value !== '' && ctx?.value !== 'isNull'));

  const addCtx = ({ _r, _e, filterText }) => {
    const newOption = { value: filterText, label: filterText };
    setContextOptions([newOption, ...contextOptions]);
    change('ctx', [newOption]);
  };

  const renderAddCtx = ({ filterValue, exactMatch }) => {
    if (!filterValue || exactMatch) {
      return null;
    } else {
      return kintIntl.formatKintMessage(
        {
          id: 'customProperties.ctx.addContext',
          overrideValue: labelOverrides?.addCtx
        },
        {
          value: filterValue
        }
      );
    }
  };

  const primaryRetiredValidator = (_v, allValues) => {
    if (allValues.primary && allValues.retired) {
      return kintIntl.formatKintMessage(
        {
          id: 'customProperties.errors.primaryRetired',
          overrideValue: labelOverrides?.primaryRetired
        }
      );
    }
    return null;
  };

  return (
    <>
      {/* Users can only configure the type of a custom property when creating it, not when editing it */}
      <Row>
        <Col xs={6}>
          <Field
            component={Select}
            dataOptions={[
              { label: '', value: '' },
              {
                label: kintIntl.formatKintMessage({
                  id: `customProperties.type.${DECIMAL_CLASS_NAME}`,
                  overrideValue: labelOverrides?.[DECIMAL_CLASS_NAME]
                }),
                value: DECIMAL_CLASS_NAME,
              },
              {
                label: kintIntl.formatKintMessage({
                  id: `customProperties.type.${INTEGER_CLASS_NAME}`,
                  overrideValue: labelOverrides?.[INTEGER_CLASS_NAME]
                }),
                value: INTEGER_CLASS_NAME,
              },
              {
                label: kintIntl.formatKintMessage({
                  id: `customProperties.type.${TEXT_CLASS_NAME}`,
                  overrideValue: labelOverrides?.[TEXT_CLASS_NAME]
                }),
                value: TEXT_CLASS_NAME,
              },
              {
                label: kintIntl.formatKintMessage({
                  id: `customProperties.type.${REFDATA_CLASS_NAME}`,
                  overrideValue: labelOverrides?.[REFDATA_CLASS_NAME]
                }),
                value: REFDATA_CLASS_NAME,
              },
              {
                label: kintIntl.formatKintMessage({
                  id: `customProperties.type.${MULTI_REFDATA_CLASS_NAME}`,
                  overrideValue: labelOverrides?.[MULTI_REFDATA_CLASS_NAME]
                }),
                value: MULTI_REFDATA_CLASS_NAME,
              },
              {
                label: kintIntl.formatKintMessage({
                  id: `customProperties.type.${DATE_CLASS_NAME}`,
                  overrideValue: labelOverrides?.[DATE_CLASS_NAME]
                }),
                value: DATE_CLASS_NAME,
              },
            ]
            }
            disabled={!!values?.id}
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.type',
                    overrideValue: labelOverrides?.type
                  })
                }
                {helpPopovers?.type ?
                  <InfoPopover
                    content={helpPopovers?.type}
                  /> : null}
              </div>
            }
            name="type"
            required
            validate={requiredValidator}
          />
        </Col>
        <Col xs={6}>
          {(values?.type === REFDATA_CLASS_NAME || values?.type === MULTI_REFDATA_CLASS_NAME) && (
            <Field
              component={Select}
              dataOptions={[{ label: '', value: '' }, ...refdata]}
              disabled={!!values?.id}
              label={
                <div>
                  {
                    kintIntl.formatKintMessage({
                      id: 'customProperties.category',
                      overrideValue: labelOverrides?.category
                    })
                  }
                  {helpPopovers?.category ?
                    <InfoPopover
                      content={helpPopovers?.category}
                    /> : null}
                </div>
              }
              name="category"
              required
              validate={requiredValidator}
            />
          )}
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Field
            component={TextField}
            id="custom-prop-label"
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.label',
                    overrideValue: labelOverrides?.label
                  })
                }
                {helpPopovers?.label ?
                  <InfoPopover
                    content={helpPopovers?.label}
                  /> : null}
              </div>
            }
            name="label"
            required
            validate={requiredValidator}
          />
        </Col>
        <Col xs={6}>
          <Field
            component={TextField}
            id="custom-prop-name"
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.name',
                    overrideValue: labelOverrides?.name
                  })
                }
                {helpPopovers?.name ?
                  <InfoPopover
                    content={helpPopovers?.name}
                  /> : null}
              </div>
            }
            name="name"
            required
            validate={v => {
              if (v && v.length) {
                return /^[a-z][a-z0-9]*$/i.test(v) ? (
                  undefined
                ) :
                kintIntl.formatKintMessage({
                  id: 'errors.hasNonAlphaName',
                  overrideValue: labelOverrides?.hasNonAlphaName
                });
              }

              return requiredValidator(v);
            }}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={6}>
          <Field
            component={TextArea}
            id="custom-prop-description"
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.description',
                    overrideValue: labelOverrides?.description
                  })
                }
                {helpPopovers?.description ?
                  <InfoPopover
                    content={helpPopovers?.description}
                  /> : null}
              </div>
            }
            name="description"
            required
            // FIXME I think the required Validator might pose translation problems
            validate={requiredValidator}
          />
        </Col>
        <Col xs={6}>
          <Field
            actions={[{ onSelect: addCtx, render: renderAddCtx }]}
            component={MultiSelection}
            dataOptions={contextOptions}
            emptyMessage={
              kintIntl.formatKintMessage({
                id: 'customProperties.noCtxFound',
                overrideValue: labelOverrides?.noCtxFound
              })
            }
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.ctx',
                    overrideValue: labelOverrides?.ctx
                  })
                }
                {helpPopovers?.ctx ?
                  <InfoPopover
                    content={helpPopovers?.ctx}
                  /> : null}
              </div>
            }
            name="ctx"
            onChange={(selectedItems) => {
              if (selectedItems.length) {
                change('ctx', [selectedItems?.[selectedItems?.length - 1]]);
              } else {
                change('ctx', undefined);
              }
            }}
          />
        </Col>
      </Row>
      <Row>
        <Col xs={3}>
          <Field
            component={NumberField}
            id="custom-prop-weight"
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.weight',
                    overrideValue: labelOverrides?.weight
                  })
                }
                {helpPopovers?.weight ?
                  <InfoPopover
                    content={helpPopovers?.weight}
                  /> : null}
              </div>
            }
            name="weight"
            required
            validate={requiredValidator}
          />
        </Col>
        <Col xs={3}>
          <Field
            component={Select}
            dataOptions={[
              {
                label: kintIntl.formatKintMessage({
                  id: 'yes',
                  overrideValue: labelOverrides?.yes
                }),
                value: 'true',
              },
              {
                label: kintIntl.formatKintMessage({
                  id: 'no',
                  overrideValue: labelOverrides?.no
                }),
                value: 'false',
              },
            ]}
            format={booleanToString}
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.primary',
                    overrideValue: labelOverrides?.primary
                  })
                }
                {helpPopovers?.primary ?
                  <InfoPopover
                    content={helpPopovers?.primary}
                  /> : null}
              </div>
            }
            name="primary"
            parse={stringToBoolean}
            required
            validate={composeValidators(
              requiredValidator,
              primaryRetiredValidator
            )}
          />
        </Col>
        <Col xs={3}>
          <Field
            component={Select}
            dataOptions={[
              {
                label: kintIntl.formatKintMessage({
                  id: 'yes',
                  overrideValue: labelOverrides?.yes
                }),
                value: 'true',
              },
              {
                label: kintIntl.formatKintMessage({
                  id: 'no',
                  overrideValue: labelOverrides?.no
                }),
                value: 'false',
              },
            ]}
            format={booleanToString}
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.retired',
                    overrideValue: labelOverrides?.retired
                  })
                }
                {helpPopovers?.retired ?
                  <InfoPopover
                    content={helpPopovers?.retired}
                  /> : null}
              </div>
            }
            name="retired"
            parse={stringToBoolean}
            required
            validate={composeValidators(
              requiredValidator,
              primaryRetiredValidator
            )}
          />
        </Col>
        <Col xs={3}>
          <Field
            component={Select}
            dataOptions={[
              {
                label: kintIntl.formatKintMessage({
                  id: 'customProperties.internalTrue',
                  overrideValue: labelOverrides?.internalTrue
                }),
                value: 'true',
              },
              {
                label: kintIntl.formatKintMessage({
                  id: 'customProperties.internalFalse',
                  overrideValue: labelOverrides?.internalFalse
                }),
                value: 'false',
              },
            ]}
            format={booleanToString}
            label={
              <div>
                {
                  kintIntl.formatKintMessage({
                    id: 'customProperties.defaultVisibility',
                    overrideValue: labelOverrides?.defaultVisibility
                  })
                }
                {helpPopovers?.defaultVisibility ?
                  <InfoPopover
                    content={helpPopovers?.defaultVisibility}
                  /> : null}
              </div>
            }
            name="defaultInternal"
            parse={stringToBoolean}
            required
            validate={requiredValidator}
          />
        </Col>
      </Row>
    </>
  );
};

CustomPropertyForm.propTypes = {
  contextFilterOptions: PropTypes.arrayOf(PropTypes.object),
  helpPopovers: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  refdata: PropTypes.arrayOf(PropTypes.object)
};

export default CustomPropertyForm;
