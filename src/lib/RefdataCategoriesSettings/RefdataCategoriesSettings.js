import { useState } from 'react';
import PropTypes from 'prop-types';

import { Button, Col, KeyValue, Pane, PaneHeader, PaneMenu, Row } from '@folio/stripes/components';

import { useActionListRef, useKintIntl } from '../hooks';

import EditableRefdataCategoryList from '../EditableRefdataCategoryList';
import EditableRefdataList from '../EditableRefdataList';
import FormattedKintMessage from '../FormattedKintMessage';


const propTypes = {
  afterQueryCalls: PropTypes.object,
  catchQueryCalls: PropTypes.object,
  displayConditions: PropTypes.shape({
    create: PropTypes.bool,
    delete: PropTypes.bool,
    view: PropTypes.bool,
  }),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  labelOverrides: PropTypes.object,
  onClose: PropTypes.func,
  refdataEndpoint: PropTypes.string
};

const RefdataCategoriesSettings = ({
  /*
   * Set of extra booleans for controlling access to actions
   * create/delete (View should be handled externally)
   * This will not overwrite "internal" behaviour, ie setting
   * delete to 'true' here would still not render a delete button
   * for a refdata category that has refdata values.
   */
  displayConditions = {
    create: true,
    delete: true,
    edit: true
  },
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {}, // An object containing translation alternatives
  onClose,
  refdataEndpoint
}) => {
  /* A component that allows for editing of refdata categories */
  const [refdataCategory, setRefdataCategory] = useState();
  const [refState, rdclRef, passedRef] = useActionListRef();

  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  const handleRefdataCategoryClick = (rc) => {
    setRefdataCategory(rc);
  };

  const pickListValuesLabel = kintIntl.formatKintMessage({
    id: 'settings.pickListValues',
    overrideValue: labelOverrides?.pickListValues
  });

  const renderViewPaneHeader = renderProps => (
    <PaneHeader
      {...renderProps}
      dismissible
      onClose={() => setRefdataCategory()}
      paneTitle={
        kintIntl.formatKintMessage(
          {
            id: 'refdataCategories.config.viewPaneTitle',
            overrideValue: labelOverrides?.viewPaneTitle,
            fallbackMessage: refdataCategory?.desc,
          },
          {
            name: refdataCategory?.desc,
          }
        )
      }
    />
  );

  const renderLookupPaneHeader = renderProps => (
    <PaneHeader
      {...renderProps}
      dismissible
      lastMenu={
        <PaneMenu>
          <Button
            disabled={!!refState?.editing}
            marginBottom0
            onClick={() => rdclRef?.current?.create()}
          >
            <FormattedKintMessage id="new" />
          </Button>
        </PaneMenu>
      }
      onClose={onClose}
      paneTitle={kintIntl.formatKintMessage({
        id: 'settings.pickLists',
        overrideValue: labelOverrides?.pickLists
      })}
    />
  );

  return (
    <>
      <Pane
        defaultWidth="fill"
        id="edit-refdata-desc"
        renderHeader={renderLookupPaneHeader}
      >
        <EditableRefdataCategoryList
          ref={passedRef}
          afterQueryCalls={{
            post: (json) => { setRefdataCategory(json); }
          }}
          displayConditions={displayConditions}
          handleRefdataCategoryClick={handleRefdataCategoryClick}
          hideCreateButton
          onConfirmDelete={(id) => {
            if (refdataCategory?.id === id) {
              setRefdataCategory();
            }
          }}
          refdataEndpoint={refdataEndpoint}
        />
      </Pane>
      {refdataCategory?.desc &&
        <Pane
          defaultWidth="fill"
          id="settings-refdataCategories-viewPane"
          renderHeader={renderViewPaneHeader}
        >
          <Row>
            <Col xs={8}>
              <KeyValue
                label={kintIntl.formatKintMessage({
                  id: 'refdataCategory.refdataCategory',
                  overrideValue: labelOverrides?.refdataCategory
                })}
                value={refdataCategory.desc}
              />
            </Col>
            <Col xs={4}>
              <KeyValue
                label={kintIntl.formatKintMessage({
                  id: 'refdataCategory.noOfValues',
                  overrideValue: labelOverrides?.noOfValues
                })}
                value={refdataCategory?.values?.length}
              />
            </Col>
          </Row>
          <EditableRefdataList
            afterQueryCalls={{
              put: json => setRefdataCategory(json),
              delete: (json) => setRefdataCategory(json)
            }}
            desc={refdataCategory?.desc}
            displayConditions={displayConditions}
            label={pickListValuesLabel}
            refdataEndpoint={refdataEndpoint}
          />
        </Pane>
      }
    </>
  );
};

RefdataCategoriesSettings.propTypes = propTypes;

export default RefdataCategoriesSettings;
