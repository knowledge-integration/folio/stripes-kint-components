import { forwardRef, useCallback, useEffect, useImperativeHandle, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import {
  Button,
  Dropdown,
  DropdownButton,
  DropdownMenu,
  nativeChangeFieldValue as nativeChangeField,
  Label,
} from '@folio/stripes/components';

import css from '../../../styles/RichSelect.css';
import FormattedKintMessage from '../FormattedKintMessage';

const RichSelect = forwardRef(({
  ariaLabel,
  disabled = false,
  dropdownProps = {},
  id,
  input: formInput = {},
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  label,
  meta,
  onChange,
  options: userOptions = [],
  placeholder,
  renderMenu,
  renderOption,
  renderTrigger,
  required = false,
  value
}, ref) => {
  const hiddenInput = useRef(null);

  const [internalValue, setInternalValue] = useState(meta?.initial ?? value ?? '');

  const findSelectedOptionByValue = useCallback((val) => {
    return userOptions.find(opt => opt.value === val);
  }, [userOptions]);

  const [selectedOption, setSelectedOption] = useState(findSelectedOptionByValue(meta?.initial ?? '') ?? {});

  useEffect(() => {
    // We treat non-undefined value/formInput values as valid values, whereas undefined is only relevant when prop has not been provided
    if (value !== undefined && !(value === selectedOption?.value && value === internalValue)) {
      setSelectedOption(findSelectedOptionByValue(value));
      setInternalValue(value);
    } else if (formInput?.value !== undefined && !(value === selectedOption?.value && value === internalValue)) {
      setSelectedOption(findSelectedOptionByValue(formInput.value));
      setInternalValue(formInput.value);
    }
  }, [formInput, value, internalValue, selectedOption, findSelectedOptionByValue]);

  // Way to grab internal selectedOption state, thereby avoiding having to reparse options for it
  useImperativeHandle(ref, () => ({
    selectedOption
  }));

  const defaultRenderTrigger = ({ onToggle, triggerRef, keyHandler, ariaProps, getTriggerProps }) => {
    return (
      <DropdownButton
        ref={triggerRef}
        buttonClass={css.triggerButton}
        buttonStyle="none"
        disabled={disabled}
        marginBottom0
        onClick={onToggle}
        onKeyDown={keyHandler}
        paddingSide0
        type="button"
        {...getTriggerProps()}
        {...ariaProps}
      >
        {
          selectedOption?.label ??
          selectedOption?.value ??
          (internalValue || null) ??
          placeholder ??
          <FormattedKintMessage
            id="pleaseSelectOption"
            intlKey={passedIntlKey}
            intlNS={passedIntlNS}
          />
        }
      </DropdownButton>
    );
  };

  const handleChange = (e) => {
    // Actually set the value in the form (If formInput exists)
    if (formInput?.onChange) {
      formInput.onChange(e);
    }

    // If the user has set up an onChange, this will ensure that that fires
    if (onChange) {
      onChange(e, e.target.value);
    }
  };

  const changeField = (val) => {
    nativeChangeField(hiddenInput, false, val);
  };

  const defaultRenderOption = (opt) => (opt.label ?? opt.value);

  const defaultRenderMenu = ({ onToggle }) => (
    <DropdownMenu>
      {userOptions.map(opt => {
        return (
          <Button
            autoFocus={selectedOption?.value === opt.value}
            buttonStyle="dropdownItem"
            id={`${id}-${formInput.name}-option-${opt.value}`}
            marginBottom0
            onClick={() => {
              // Trigger change event
              changeField(opt.value);

              // Close the menu on select
              onToggle();
            }}
          >
            {renderOption ? renderOption(opt) : defaultRenderOption(opt)}
          </Button>
        );
      })}
    </DropdownMenu>
  );

  return (
    <>
      {(label || ariaLabel) ? (
        <Label
          className={ariaLabel && 'sr-only'}
          htmlFor={id}
          id={`${id}-label`}
          required={required}
        >
          {ariaLabel || label}
        </Label>)
        : ''
      }
      <Dropdown
        disabled={disabled}
        hasPadding
        placement="bottom-start"
        renderMenu={(menuProps) => {
          if (renderMenu) {
            return renderMenu({ ...menuProps, selectedOption, changeField });
          } else {
            return defaultRenderMenu(menuProps);
          }
        }}
        renderTrigger={(triggerProps) => {
          if (renderTrigger) {
            return renderTrigger({ ...triggerProps, selectedOption });
          } else {
            return defaultRenderTrigger(triggerProps);
          }
        }}
        {...dropdownProps}
      />
      <input
        {...formInput}
        ref={hiddenInput}
        hidden
        onChange={handleChange}
        type="text"
        value={internalValue}
      />
    </>
  );
});

RichSelect.propTypes = {
  ariaLabel: PropTypes.string,
  disabled: PropTypes.bool,
  dropdownProps: PropTypes.object,
  id: PropTypes.string,
  input: PropTypes.object,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
    PropTypes.node
  ]),
  meta: PropTypes.object,
  onChange: PropTypes.func,
  options: PropTypes.arrayOf(PropTypes.shape({
    value: PropTypes.string.isRequired,
    label: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.node
    ])
  })),
  placeholder: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.node
  ]),
  renderMenu: PropTypes.func,
  renderOption: PropTypes.func,
  renderTrigger: PropTypes.func,
  required: PropTypes.bool,
  value: PropTypes.string,
};

export default RichSelect;
