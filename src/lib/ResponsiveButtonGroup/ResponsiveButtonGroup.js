import { cloneElement, useCallback, useMemo } from 'react';

import PropTypes from 'prop-types';
import classnames from 'classnames';

import {
  Button,
  Dropdown,
  DropdownMenu,
  Icon
} from '@folio/stripes/components';

import css from '../../../styles/ResponsiveButtonGroup.css';

import useResponsiveButtonGroupSizing from './useResponsiveButtonGroupSizing';

const propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  dropdownTriggerProps: PropTypes.object,
  fullWidth: PropTypes.bool,
  id: PropTypes.string,
  marginBottom0: PropTypes.bool,
  selectedIndex: PropTypes.number,
  tagName: PropTypes.string,
};

const SIZE_OF_DOWN_ARROW_BUTTON = 46;
const calculateCumulativeThreshold = (arrayOfNumbers, maximum, index = 0, value = 0) => {
  // If all the numbers fit, return the last index
  if (arrayOfNumbers.reduce((partialSum, a) => partialSum + a, 0) <= maximum) {
    return arrayOfNumbers?.length - 1;
  }

  // Else we'll need a down arrow button, so include that in calculations
  const nextValue = value + arrayOfNumbers[index];
  if (nextValue + SIZE_OF_DOWN_ARROW_BUTTON <= maximum && (index + 1 < arrayOfNumbers?.length)) {
    return calculateCumulativeThreshold(arrayOfNumbers, maximum, index + 1, nextValue);
  }

  return index - 1;
};


const ResponsiveButtonGroup = (props) => {
  const {
    children: childButtons, // These MUST consist only of Button components
    className,
    dropdownTriggerProps = {},
    fullWidth = false, // Only bother with this when buttonGroupWidth < ContainerWidth
    id,
    marginBottom0, // Will ONLL control the margin of the dropdown button
    selectedIndex,
    tagName: Tag = 'div',
    ...rest
  } = props;

  const {
    buttons,
    cachedSizeArray,
    containerRef,
    containerWidth,
    ready,
  } = useResponsiveButtonGroupSizing({
    buttons: childButtons,
    id
  });

  const buttonThreshold = calculateCumulativeThreshold(cachedSizeArray, containerWidth);

  const renderActionMenuToggle = useCallback(({ displayButtons, onToggle, triggerRef, keyHandler, open, ariaProps, getTriggerProps }) => (
    <Button
      ref={triggerRef}
      aria-label="menu"
      buttonClass={
        classnames(
          css.width100,
          { [`${css.dropdownButtonClass}`]: (displayButtons?.length ?? 0) > 0 },
          { [`${css.marginBottom}`]: !marginBottom0 },
          { [`${css.marginBottom0}`]: marginBottom0 }
        )
      }
      buttonStyle={selectedIndex > buttonThreshold ? 'primary' : 'default'}
      onClick={onToggle}
      onKeyDown={keyHandler}
      type="button"
      {...getTriggerProps()}
      {...ariaProps}
      {...dropdownTriggerProps}
    >
      <Icon icon={open ? 'triangle-up' : 'triangle-down'} iconPosition="end" />
    </Button>
  ), [buttonThreshold, dropdownTriggerProps, marginBottom0, selectedIndex]);

  const displayButtons = useMemo(() => (
    buttons?.slice(0, buttonThreshold + 1)?.map((button, index) => {
      if (index === selectedIndex && !button?.buttonStyle) {
        return cloneElement(button, { buttonStyle: 'primary' });
      }

      return button;
    })
  ), [buttonThreshold, buttons, selectedIndex]);

  const dropdownButtons = useMemo(() => (
    [...buttons
      .slice(buttonThreshold + 1, buttons?.length)
      ?.map(button => {
        return cloneElement(button, { buttonStyle: 'dropdownItem' });
      })
    ]
  ), [buttonThreshold, buttons]);

  // eslint-disable-next-line react/prop-types
  const renderActionMenuContent = useCallback(() => (
    <DropdownMenu>
      {[...dropdownButtons]}
    </DropdownMenu>
  ), [dropdownButtons]);

  const buttonsSlice = useMemo(() => (
    [
      ...displayButtons,
      <Dropdown
        key="responsiveButtonGroup-dropdown-toggle"
        className={css.dropdownClass}
        renderMenu={renderActionMenuContent}
        renderTrigger={(triggerProps) => renderActionMenuToggle({ displayButtons, ...triggerProps })}
      />
    ]
  ), [displayButtons, renderActionMenuContent, renderActionMenuToggle]);

  return (
    <>
      <div ref={containerRef} className="width: 100%;" />
      <Tag
        className={
          classnames(
            { [`${css.hidden}`]: !ready },
            css.buttonGroup,
            { [`${css.fullWidth}`]: ready && fullWidth },
            className
          )
        }
        style={fullWidth ? { width: `${containerWidth}px` } : {}}
        {...rest}
      >
        {!ready || buttonThreshold === cachedSizeArray?.length - 1 ?
          buttons?.map((button, index) => {
            if (index === selectedIndex && !button?.buttonStyle) {
              return cloneElement(button, { buttonStyle: 'primary' });
            }

            return button;
          }) :
          buttonsSlice
        }
      </Tag>
    </>
  );
};

ResponsiveButtonGroup.propTypes = propTypes;

export default ResponsiveButtonGroup;
