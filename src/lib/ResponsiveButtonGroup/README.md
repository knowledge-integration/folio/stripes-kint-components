# ResponsiveButtonGroup
A component designed to render a buttongroup responsively, measuring when a container is too narrow to fully contain it, and thus creating a dropdown menu button instead.

## Basic Usage
```
import { Button } from '@folio/stripes/components
import { ResponsiveButtonGroup } from '@k-int/stripes-kint-components';

...
<ResponsiveButtonGroup>
  <Button>
    Button 1
  </Button>
  <Button>
    Button 2
  </Button>
</ResponsiveButtonGroup>
```

## Props
This component aims to stay in line with the stripes-component `ButtonGroup`, documentation found [here](https://github.com/folio-org/stripes-components/blob/master/lib/ButtonGroup/readme.md).

The only extra property is `selectedIndex`.

Name | type | description | default | required
--- | --- | --- | --- | ---
children | | Set of `<Button>`s. |  |
className | string | Add a custom className to ButtonGroup | |
dropdownTriggerProps | object | Extra props to pass to the Stripes Button which triggers the dropdown menu. | |
fullWidth | bool | Forces the button group width to 100% |
selectedIndex | integer | Allows the implementor to pass off "primary" style management to the component. This will style the requisite index as 'primary', if that index is not hidden, or the dropdown menu if it is. |
tagName | string | Set the HTML tag used to wrap the button set. | 'div' |
