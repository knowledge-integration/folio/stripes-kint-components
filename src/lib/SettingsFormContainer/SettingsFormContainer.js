import PropTypes from 'prop-types';

import { useCallout } from '@folio/stripes/core';
import { useKintIntl, useModConfigEntries, useMutateModConfigEntry } from '../hooks';

/*
 * API is similar to ConfigManager
 */
const SettingsFormContainer = ({
  ConfigFormComponent,
  configName,
  getInitialValues: passedGetInitialValues,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  moduleName,
  scope, // We don't use this currently but it's here for future reference
  ...rest
}) => {
  const callout = useCallout();
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

/*
  * It is important to pass a namespaceAppend because the same hook could be rendered
  * on the main App component. This means that in settings
  * (Which is rendered inside App itself, stripes behaviour)
  * the same query namespace would apply to the query in the config form, and also to
  * the App level hook call, resulting in an infinite loop.
  *
  * We avoid this by ensuring all calls from this component have (at least) an extra namespace key item
  */
  const {
    parsedSettings,
    settings
  } = useModConfigEntries({
    moduleName,
    configName,
    namespaceAppend: ['SettingsFormContainer']
  });

  const { mutateAsync: editSettings } = useMutateModConfigEntry({
    configName,
    moduleName,
    id: settings?.id
  });

  const onSubmit = (values) => {
    const setting = {
      ...settings,
      value: JSON.stringify(values),
      ...(moduleName ?
        { module: moduleName, configName } :
        { scope, key: configName })
    };

    editSettings(setting).then(() => {
      callout.sendCallout({
        message: kintIntl.formatKintMessage({
          fallbackMessage: 'stripes-smart-components.cm.success',
          id: 'settingSaveSuccess',
          intlKey: passedIntlKey,
          intlNS: passedIntlNS,
          overrideValue: labelOverrides?.settingSaveSuccess
        }),
        type: 'success',
      });
    });

    // At some point we might want to add a catch callout.
    // The current ConfigManager doesn't have one, so leaving for now
  };

  const getInitialValues = () => {
    let initialValues = parsedSettings;
    if (passedGetInitialValues) {
      initialValues = passedGetInitialValues(parsedSettings, settings);
    }

    return initialValues;
  };

  return (
    <div style={{ width: '100%' }}>
      <ConfigFormComponent
        initialValues={getInitialValues()}
        onSubmit={onSubmit}
        {...rest}
      />
    </div>
  );
};

SettingsFormContainer.propTypes = {
  ConfigFormComponent: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.func,
  ]),
  configName: PropTypes.string.isRequired,
  getInitialValues: PropTypes.func,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  moduleName: PropTypes.string.isRequired,
  scope: PropTypes.string,
};

export default SettingsFormContainer;
