import PropTypes from 'prop-types';
import { FormattedMessage, useIntl } from 'react-intl';
import { useIntlKey } from '../hooks';

// This works analogously to useKintIntl's "formatKintMessage"
const FormattedKintMessage = ({
  fallbackMessage,
  id,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  overrideValue,
  ...formattedMessageProps
}) => {
  const intlKey = useIntlKey(passedIntlKey, passedIntlNS);
  const intlObj = useIntl();

  if (overrideValue) {
    // Version 3 is a breaking change, where labelOverrides must be strings.
    if (typeof overrideValue !== 'string') {
      throw new Error('Override values must be strings as of stripes-kint-components ^3.0.0');
    }

    if (intlObj.messages?.[overrideValue]) {
      // We've been passed a key as an override, return formattedMessage with it
      return <FormattedMessage id={overrideValue} {...formattedMessageProps} />;
    }
    // At this stage we have an overrideValue that's not a key, return it.
    return overrideValue;
  }

  // If key does not exist in intl, and we have a specified "fallbackMessage", use that

  // (FallbackMessage works like defaultMessage but with no warning)
  if (!intlObj.messages?.[`${intlKey}.${id}`] && fallbackMessage) {
    // Allow fallback message to be an intl key
    if (intlObj.messages?.[fallbackMessage]) {
      return <FormattedMessage id={fallbackMessage} {...formattedMessageProps} />;
    }

    return fallbackMessage;
  }

  return (<FormattedMessage id={`${intlKey}.${id}`} {...formattedMessageProps} />);
};

FormattedKintMessage.propTypes = {
  fallbackMessage: PropTypes.string,
  id: PropTypes.string.isRequired,
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  overrideValue: PropTypes.string
};

export default FormattedKintMessage;
