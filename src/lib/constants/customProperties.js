export const TYPE_CLASS_PREFIX = 'com.k_int.web.toolkit.custprops.types.CustomProperty';
export const BLOB_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyBlob';
export const BOOLEAN_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyBoolean';
export const CONTAINER_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyContainer';
export const DECIMAL_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyDecimal';
export const INTEGER_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyInteger';
export const REFDATA_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdata';
export const MULTI_REFDATA_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyMultiRefdata';
export const REFDATA_DEFINITION_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyRefdataDefinition';
export const TEXT_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyText';
export const DATE_CLASS_NAME = 'com.k_int.web.toolkit.custprops.types.CustomPropertyLocalDate';

