export default [
  '==',
  '!=',
  '=~',
  '!~',
  '=i=',
  /*
   * The following comparators include a space, it is _important_ that this space is here.
   * Note that this defines the "default" usage of comparators to be UNSPACED
   */
  ' isEmpty',
  ' isNotEmpty',
  ' isSet',
  ' isNotSet',
  ' isNull',
  ' isNotNull',
];
