export default {
  filterKeys: {
    DescKey: 'desc'
  },
  sort: [{ path: 'desc' }],
  stats: false,
  max: 100
};
