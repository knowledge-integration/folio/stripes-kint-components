import comparators from '../../constants/comparators';

/*
 * This is designed to take a SINGLE query string of the form
 * attribute == value, or attribute isEmpty, or attribute ~= value, etc
 *
 * Will return an object: {path: 'attribute', comparator: '==', value: 'value'}
 */
// No magic numbers sonarlint -.-
const FIRST = 0;
const SECOND = 1;
const THIRD = 2;

const parseKiwtQueryString = (queryString) => {
  const comparatorMatch = new RegExp(`(${comparators.join('|')})`);
  // See parseKiwtQueryGroups for breakdown of why this filter is needed
  const splitString = queryString.split(comparatorMatch)
                                 .filter(Boolean);

  /*
   * As far as I can tell, with input of a SINGLE query string at a time,
   * with 2 entries we will have a special case comparator, with 3 we
   * have a comparator, attribute and value.. value can be null in those cases
   */
  return ({
    path: splitString[FIRST],
    comparator: splitString[SECOND],
    value: splitString[THIRD]
  });
};

export default parseKiwtQueryString;
