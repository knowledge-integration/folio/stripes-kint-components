// Does not validate object... make sure you pass it the right thing
export const deparseKiwtQueryFiltersObject = (queryObject) => {
  let returnString = `${queryObject.path}${queryObject.comparator}`;
  if (queryObject.value) {
    returnString += `${queryObject.value}`;
  }

  return returnString;
};

// Takes an Array of the shape defined by parseKiwtQuery and restructures that into an equivalent KIWT queryString
const deparseKiwtQueryFilters = (queryArray) => {
  if (Array.isArray(queryArray)) {
    let returnString = '';
    queryArray.forEach(qa => {
      if (Array.isArray(qa)) {
        returnString += `(${deparseKiwtQueryFilters(qa)})`;
      } else if (typeof qa === 'string') {
        returnString += qa;
      } else {
        // Assuming object at this stage
        returnString += `${deparseKiwtQueryFiltersObject(qa)}`;
      }
    });

    return returnString.trim();
  } else {
    throw new Error(`deparseKiwtQuery expects a parameter of type Array, passed: ${queryArray}`);
  }
};

export default deparseKiwtQueryFilters;
