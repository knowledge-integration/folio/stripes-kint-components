import { generateKiwtQueryParams } from './generateKiwtQueryParams';

const generateKiwtQuery = (options, nsValues, encode = true) => {
  const paramsArray = generateKiwtQueryParams(options, nsValues, encode);
  return paramsArray.length ? '?' + paramsArray.join('&') : '';
};

export default generateKiwtQuery;
