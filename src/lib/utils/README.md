# Utility Functions

## generateKiwtQuery

The `generateKiwtQuery` function generates a URL query string in "KIWT" (K-Int Web Toolkit) style from a SASQ (Search and Sort Query) object. It handles search terms, filters, sorting, and other parameters, making it easy to construct complex query strings for backend APIs.
This component calls and then joins the output of `generateKiwtQueryParams`. For more granular information on how this works, see the generateKiwtQueryParams README.
### Basic Usage

```javascript
import { generateKiwtQuery } from '@k-int/stripes-kint-components';

const nsValues = {
  query: 'test',
  filters: 'requestStatus.completed,journalVolume.test1,startDate.startDate>=2021-10-28',
  sort: '-title' // Example of sorting by title in descending order
};

const options = {
  searchKey: 'request.name',
  filterKeys: {
    requestStatus: 'requestStatus.value',
    journalVolume: 'journalVolume'
  },
  sortKeys: {
    title: 'item.title' // Example of mapping 'title' to 'item.title' for sorting
  }
};

const queryString = generateKiwtQuery(options, nsValues);

// queryString will be:
//?match=request.name&term=test&filters=requestStatus.value==completed&filters=journalVolume==test1&filters=startDate%3E=2021-10-28&sort=item.title;desc&stats=true
```

### Props

| Name | Type | Description | Default | Required |
|---|---|---|---|---|
| options | object | An object with keys: `searchKey`, `filterKeys`, `sortKeys`, and `stats`, which maps the incoming `nsValues` object to a KIWT query. You can also pass arbitrary `key`/`value` pairs to append `key==value` onto the query. |  | ✓ |
| nsValues | object | An object containing the query parameters.  Can contain `query`, `filters`, and `sort`, as well as any other arbitrary parameters. |  | ✓ |
| encode | boolean | A boolean indicating whether to URL-encode the values. | `true` | ✕ |
