const modConfigEntriesQueryKey = ({
  configName,
  moduleName,
  namespaceAppend
} = {}) => {
  const namespace = ['stripes-kint-components', 'modConfigSettings', moduleName, configName];

  if (Array.isArray(namespaceAppend)) {
    namespaceAppend.forEach(appendItem => {
      namespace.push(appendItem);
    });
  } else if (namespaceAppend) {
    namespace.push(namespaceAppend);
  }

  return namespace;
};

export default modConfigEntriesQueryKey;
