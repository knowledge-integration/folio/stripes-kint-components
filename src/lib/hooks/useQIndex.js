import React, { useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import queryString from 'query-string';

const useQIndex = () => {
  const history = useHistory();
  const location = useLocation();

  const query = queryString.parse(location.search);
  const [currentQindex, setCurrentQindex] = useState(query?.qindex);

  const handleQindexChange = (newQindex) => {
    setCurrentQindex(newQindex);
    if (newQindex !== query?.qindex) {
      let newQuery;
      if (newQindex !== '') {
        newQuery = {
          ...query,
          qindex: newQindex
        };
      } else {
        newQuery = {
          ...query,
          qindex: undefined
        };
      }

      history.push({
        pathname: location.pathname,
        search: `?${queryString.stringify(newQuery)}`
      });
    }
  };

  useEffect(() => {
    if (currentQindex !== query?.qindex) {
      setCurrentQindex(query?.qindex);
    }
  }, [
    currentQindex,
    history,
    location,
    query
  ]);

  return [currentQindex, handleQindexChange];
};

export default useQIndex;
