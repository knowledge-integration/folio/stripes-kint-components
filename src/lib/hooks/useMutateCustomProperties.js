import useMutateGeneric from './useMutateGeneric';

const useMutateCustomProperties = ({
  endpoint,
  id,
  ...mutateGenericProps
}) => {
  return useMutateGeneric({
    endpoint,
    endpointMutators: {
      delete: () => `${endpoint}/${id}`,
      put: () => `${endpoint}/${id}`,
    },
    id,
    queryKey: ['stripes-kint-components', 'useMutateCustomProperties', id],
    ...mutateGenericProps
  });
};

export default useMutateCustomProperties;
