
import { useMutation, useQueryClient } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';

import { modConfigEntriesQueryKey } from '../utils';
import { MOD_SETTINGS_ENDPOINT } from '../constants/endpoints';

// This will simply take in some information and decide whether to mutate via POST or via PUT
const useMutateModConfigEntry = ({
  configName,
  moduleName,
  id
}) => {
  const ky = useOkapiKy();
  const queryClient = useQueryClient();

  const baseNamespace = modConfigEntriesQueryKey({
    configName,
    moduleName
  });

  const putQueryObject = useMutation(
    [...baseNamespace, 'putConfigEntry', id],
    async (data) => ky.put(
      `${MOD_SETTINGS_ENDPOINT}/${id}`,
      { json: data }
    ).json()
      .then(() => {
        queryClient.invalidateQueries(baseNamespace);
      })
  );

  const postQueryObject = useMutation(
    [...baseNamespace, 'postConfigEntry'],
    async (data) => ky.post(
      `${MOD_SETTINGS_ENDPOINT}`,
      { json: data }
    ).json()
      .then(() => {
        queryClient.invalidateQueries(baseNamespace);
      })
  );

  if (id) {
    return putQueryObject;
  }

  return postQueryObject;
};

export default useMutateModConfigEntry;
