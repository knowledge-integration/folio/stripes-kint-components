import PropTypes from 'prop-types';

import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';

import { generateKiwtQuery, refdataOptions, refdataQueryKey } from '../utils';

const useRefdata = ({
  endpoint,
  desc,
  options = refdataOptions,
  queryParams,
  returnQueryObject = false,
}) => {
  const ky = useOkapiKy();

  const nsValues = {};

  /* Desc will tend to contain a '.', which will throw off generateKiwtQuery.
   * To combat this we can insert another Desc. at the beginning to act as the filtername, so
   * Funder.Name => Desc.desc=Funder.Name. This way "Desc" acts as the filterName, and without a filterKey
   * "desc=Funder.Name" is passed wholesale to the query.
   */

  if (Array.isArray(desc)) {
    // If we have an array, append a desc filter for each desc given
    nsValues.filters = desc.map(d => `DescKey.${d}`).join(',');
  } else if (desc) {
    // If we just have a string, append a single desc filter
    nsValues.filters = `DescKey.${desc}`;
  }

  const query = generateKiwtQuery(options, nsValues);
  const path = `${endpoint}${query}`;

  const queryObject = useQuery(
    refdataQueryKey(desc),
    () => ky(path).json(),
    queryParams
  );

  if (returnQueryObject) {
    return queryObject || {};
  }

  const { data: refdata } = queryObject;
  return refdata || [];
};

useRefdata.propTypes = {
  endpoint: PropTypes.string,
  desc: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)
  ]),
  queryParams: PropTypes.object,
  returnQueryObject: PropTypes.bool
};

export default useRefdata;
