import { useEffect, useState } from 'react';
import { useQuery } from 'react-query';
import { useOkapiKy } from '@folio/stripes/core';

import { typedownQueryKey } from '../../utils';

const useTypedownData = (path, callPath) => {
  const ky = useOkapiKy();

  const queryKey = typedownQueryKey(path);
  if (callPath) {
    queryKey.push(callPath);
  }

  const { data, isLoading } = useQuery(
    // Ensure when multiple apps are using this function that each one gets memoized individually
    queryKey,
    () => ky(callPath).json()
  );

  // Smooth out transitions while data changes by only displaying once call has been loaded.
  const [contentData, setContentData] = useState();
  useEffect(() => {
    if (!isLoading) {
      setContentData(data);
    }
  }, [data, isLoading]);

  return contentData;
};

export default useTypedownData;
