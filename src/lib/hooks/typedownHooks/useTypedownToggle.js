import { useCallback, useEffect, useState } from 'react';
import useActiveElement from '../useActiveElement';
import selectorSafe from '../../utils/selectorSafe';

const useTypedownToggle = (name) => {
  const [open, setOpen] = useState(false);
  const onToggle = useCallback(() => {
    setOpen(!open);
  }, [open, setOpen]);

  const { hasParent } = useActiveElement();

  // Trawl up element stack and check if a typedown-parent exists, use that to toggle
  const openBool = !open && hasParent(`typedown-parent-${selectorSafe(name)}`);
  const closeBool = open && !hasParent(`typedown-parent-${selectorSafe(name)}`);

  useEffect(() => {
    if (openBool || closeBool) {
      onToggle();
    }
  }, [closeBool, onToggle, openBool]);

  return { open, onToggle };
};

export default useTypedownToggle;
