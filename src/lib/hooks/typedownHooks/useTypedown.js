import { useEffect, useRef, useState } from 'react';
import { useResizeDetector } from 'react-resize-detector';

import {
  getFirstFocusable,
  getLastFocusable,
  getNextFocusable,
  getPreviousFocusable,
} from '@folio/stripes/components';

import {
  DOWN_ARROW,
  TAB,
  UP_ARROW
} from '../../constants/eventCodes';

import selectorSafe from '../../utils/selectorSafe';

import useTypedownToggle from './useTypedownToggle';

const useTypedown = (
  name,
  { timeout = 800 } = {}
) => {
  // SEARCHFIELD COMPONENT
  const searchFieldComponent = document.getElementById(`typedown-searchField-${selectorSafe(name)}`);

  // SET UP REFS
  const listRef = useRef();
  const triggerRef = useRef();
  const overlayRef = useRef();
  const footerRef = useRef();

  const footer = document.getElementById(`typedown-footer-${selectorSafe(name)}`);
  // Add an event listener to the footer, so that we can control tab behaviour between footer elements

  if (footer && footer.getAttribute('hasListener') !== 'true') {
    footer.addEventListener('keydown', e => {
      // We want special behaviour on tab
      if (e.code === TAB) {
        // Prevent the default behaviour
        e.preventDefault();
        const focusFunc = e.shiftKey ? getPreviousFocusable : getNextFocusable;
        const elem = focusFunc(footerRef.current, true, true, false, true);

        if (elem) {
          // Focus on next focusable element
          elem.focus();
        } else if (e.shiftKey) {
          // We are at the beginning of the list, refocus on search bar
          searchFieldComponent.focus();
        } else {
          // We are at the end of the list, move onto next focusable element in page
          getNextFocusable(searchFieldComponent, false).focus();
        }
      }
    });

    footer.setAttribute('hasListener', 'true');
  }

  // SET UP HANDLERS
  const searchFieldKeyDownHandler = e => {
    if (e.code === UP_ARROW) {
      const elem = getLastFocusable(listRef.current, true, true);
      if (elem) {
        elem.focus();
      }
    }

    if (e.code === DOWN_ARROW) {
      const elem = getFirstFocusable(listRef.current, true, true);
      if (elem) {
        elem.focus();
      }
    }

    // Tab key (But not while shifting)
    if (e.code === TAB && !e.shiftKey) {
      e.preventDefault();
      // If we have focusable elements in the footer, then focus on them, else unfocus searchbar
      const elem = getNextFocusable(footerRef.current, true, true, true, true);
      if (elem) {
        elem.focus();
      } else {
        getNextFocusable(searchFieldComponent, false).focus();
      }
    }
  };

  const listKeyDownHandler = e => {
    if (e.code === DOWN_ARROW) {
      const elem = getNextFocusable(listRef.current, true, true);
      elem.focus();
    }

    if (e.code === UP_ARROW) {
      const elem = getPreviousFocusable(listRef.current, true, true);
      elem.focus();
    }

    if (e.code === TAB) {
      e.preventDefault();
      let elem;
      if (!e.shiftKey && !footerRef.current) {
        elem = getNextFocusable(searchFieldComponent, false);
      } else if (!e.shiftKey) {
        elem = getNextFocusable(footerRef.current, true, true, true, true);
      } else {
        elem = searchFieldComponent;
      }
      elem.focus();
    }
  };

  const handleNextFocus = () => getNextFocusable(searchFieldComponent, false).focus();

  // SET UP VARIABLES
  const { open } = useTypedownToggle(name);
  const [useOpen, setUseOpen] = useState(false);

  useEffect(() => {
    // Use setTimeout to update the message after 2000 milliseconds (2 seconds)
    const timeoutId = setTimeout(() => {
      setUseOpen(true);
    }, timeout); // Wait 0.8 seconds for open prop to get used

    // Cleanup function to clear the timeout if the component unmounts
    return () => clearTimeout(timeoutId);
  }, [timeout]);

  // RESIZE STUFF
  const { width: searchWidth, ref: resizeRef } = useResizeDetector();

  // OVERLAY PORTAL
  const portal = document.getElementById('OverlayContainer');

  return {
    refs: {
      listRef,
      triggerRef,
      overlayRef,
      footerRef
    },
    handlers: {
      handleNextFocus,
      listKeyDownHandler,
      searchFieldKeyDownHandler
    },
    variables: {
      open: useOpen ? open : false,
      portal,
      resizeRef,
      searchWidth
    }
  };
};

export default useTypedown;
