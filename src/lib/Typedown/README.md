## Typedown Component

The `Typedown` component provides a typeahead/autocomplete input field.  It displays a dropdown list of suggestions as the user types, allowing them to select a value.

### Basic Usage

```javascript
import Typedown from '@k-int/stripes-kint-components';

<Typedown
  label="Select an option"
  dataOptions={[
    { id: 1, name: 'Option 1' },
    { id: 2, name: 'Option 2' },
    { id: 3, name: 'Option 3' },
  ]}
  input={{
    name: 'myField',
    value: { id: 2, name: 'Option 2' }, // Example of a selected value
    onChange: (value) => console.log(value),
  }}
/>
```

### Props

| Name                     | Type      | Description                                                                                                                                                                                                                                                                                                                              | Default         | Required |
|--------------------------|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-----------------|---|
| className                | string    | CSS class name to apply to the component's outer div.                                                                                                                                                                                                                                                                                    |                 | ✕ |
| dataOptions              | array     | An array of objects representing the dropdown options.  Each object should have a property (specified by `uniqueIdentificationPath`) that serves as a unique identifier.                                                                                                                                                                 |                 | ✓ |
| displayClearItem         | bool      | Whether to display a clear icon when an item is selected.                                                                                                                                                                                                                                                                                | `true`          | ✕ |
| displayValueWhileOpen    | bool      | Whether or not to display the "value" underneath the typedown search while the dropdown is open. Defaults to true to avoid size changing onClick.                                                                                                                                                                                        | `true`          | ✕ |
| endOfList                | node/func | Component or function to render when the dropdown list is empty. Defaults to `<EndOfList />` from `@folio/stripes/components`.                                                                                                                                                                                                           | `<EndOfList />` | ✕ |
| filterPath               | string    | Path to the property in `dataOptions` objects to use for filtering. If not provided, filtering is done on the property specified by `uniqueIdentificationPath`.                                                                                                                                                                          |                 | ✕ |
| id                       | string    | Id to apply to the component's outer div.                                                                                                                                                                                                                                                                                                |                 | ✕ |
| initialTimeoutDelay      | number    | Delay applied to `open` occurring on first render. Set to 800ms to avoid any stripes animations, such as being the first element focused in an opening modal.                                                                                                                                                                            | 800             | ✕ |
| input                    | object | An object containing the input props typically provided by a form library like `react-final-form` or `redux-form`.  Should include `name`, `value`, and `onChange`.                                                                                                                                                                      |                 | ✓ |
| isSelected               | func | A function `(inputValue, dataOption)` that determines if a given `dataOption` is currently selected. Useful when selected values are complex objects. If not provided, selection is determined by comparing the value of the `uniqueIdentificationPath` property of the `input.value` and the `dataOption`.                              |                 | ✕ |
| label                    | string/element | Label for the input field.                                                                                                                                                                                                                                                                                                               |                 | ✕ |
| meta                     | object | Meta information about the field, typically provided by a form library. Useful for displaying error messages etc.                                                                                                                                                                                                                        |                 | ✕ |
| onChange                 | func | A callback function `(value)` that is called when a value is selected from the dropdown.  This is in addition to the `input.onChange` provided.                                                                                                                                                                                          |                 | ✕ |
| onType                   | func | A callback function `(event)` that is called when the user types in the search field. Allows for custom filtering or data fetching based on user input.                                                                                                                                                                                  |                 | ✕ |
| renderFooter             | func | A function `(displayData, currentlyTyped, exactMatch)` that renders a footer below the dropdown list. Receives the currently filtered `displayData`, what the user has typed, and whether it is an exact match.                                                                                                                          |                 | ✕ |
| renderListItem           | func | A function `(option, currentlyTyped, exactMatch, optionIsSelected)` that renders each item in the dropdown list. Receives the current option, what the user has typed, whether there is an exact match, and if the option is selected. If not provided, the value of the `uniqueIdentificationPath` property of the option is displayed. |                 | ✕ |
| required                 | bool | Whether the input is required.                                                                                                                                                                                                                                                                                                           |                 | ✕ |
| selectedStyles           | string | CSS class name to apply to the selected item display.                                                                                                                                                                                                                                                                                    |                 | ✕ |
| uniqueIdentificationPath | string | Path to the property in `dataOptions` objects that serves as a unique identifier for each option and is used to determine if an option is selected.                                                                                                                                                                                      | `'id'`          | ✕ |