import React, { useCallback, useEffect, useMemo, useState } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import get from 'lodash/get';

import { EndOfList, IconButton, Popper } from '@folio/stripes/components';

import SearchField from '../SearchField';
import css from '../../../styles/TypeDown.css';

import { useTypedown } from '../hooks/typedownHooks';
import selectorSafe from '../utils/selectorSafe';

const Typedown = ({
  className,
  dataOptions,
  displayClearItem = true,
  displayValueWhileOpen = true,
  endOfList,
  id,
  initialOpenDelay = 800, // Initial opening delay of 800ms (handles any stripes animations)
  input,
  isSelected,
  filterPath,
  label,
  meta,
  onChange,
  onType,
  renderFooter = null,
  renderListItem = null,
  required,
  selectedStyles, // A way to pass any styles that need to be applied globally on selection
  uniqueIdentificationPath = 'id'
}) => {
  const selectedUniqueId = get(input.value, uniqueIdentificationPath);

  // Display data needs to be in line with data options but also able to react to default handleType
  const [displayData, setDisplayData] = useState(dataOptions);

  // keep track of what we've typed and whether we've typed an exact match or not
  const [currentlyTyped, setCurrentlyTyped] = useState('');
  const [exactMatch, setExactMatch] = useState(false);

  useEffect(() => {
    setDisplayData(dataOptions);
  }, [dataOptions]);

  // Setup default handleType
  const handleType = (e) => {
    const regex = new RegExp(`${e.target.value.toLowerCase()}`);
    if (onType) {
      onType(e);
    } else if (filterPath && e?.target?.value) {
      setDisplayData(dataOptions.filter(item => get(item, filterPath)?.toLowerCase()?.match(regex)));
    } else if (e?.target?.value) {
      setDisplayData(dataOptions.filter(item => get(item, uniqueIdentificationPath)?.toLowerCase()?.match(regex)));
    } else {
      setDisplayData(dataOptions);
    }

    setCurrentlyTyped(e.target.value);

    if (displayData.length === 1 && get(displayData[0], filterPath) === e.target.value) {
      setExactMatch(true);
    } else {
      setExactMatch(false);
    }
  };

  // Hook to set up all the essentials
  const {
    refs: {
      listRef,
      triggerRef,
      overlayRef,
      footerRef
    },
    handlers: {
      handleNextFocus,
      listKeyDownHandler,
      searchFieldKeyDownHandler
    },
    variables: {
      open,
      portal,
      resizeRef,
      searchWidth
    }
  } = useTypedown(
    input.name,
    {
      timeout: initialOpenDelay
    }
  );

  const renderItem = useCallback((option, optionIsSelected = false) => (
    <div
      className={css.listItem}
    >
      {renderListItem ?
        renderListItem(option, currentlyTyped, exactMatch, optionIsSelected) :
        get(option, uniqueIdentificationPath)
      }
    </div>
  ), [currentlyTyped, exactMatch, renderListItem, uniqueIdentificationPath]);

  const handleChange = useCallback(value => {
    input.onChange(value);

    if (typeof onChange === 'function') {
      onChange(value);
    }
  }, [input, onChange]);

  const dropDown = useCallback(() => {
    return (
      <div
        className={css.dropdownMenu}
        id={`typedown-parent-${selectorSafe(input.name)}-menu`}
        style={{ '--searchWidth': `${searchWidth}px` }}
      >
        <div
          ref={listRef}
          className={css.listContainer}
          id="typedown-list"
        >
          {displayData?.length ? displayData?.map((d, index) => {
            const isSelectedEval = isSelected ? isSelected(input.value, d) : get(input.value, uniqueIdentificationPath) === get(d, uniqueIdentificationPath);

            const selectedCSS = selectedStyles ?? css.selectedMenuButton;
            return (
              <button
                key={`typedown-button-[${index}]`}
                className={classnames(
                  css.fullWidth,
                  css.menuButton,
                  { [`${selectedCSS}`]: isSelectedEval },
                )}
                data-selected={isSelectedEval}
                id={`typedown-button-[${index}]`}
                onClick={() => {
                  handleChange(d);
                  handleNextFocus();
                }}
                onKeyDown={listKeyDownHandler}
                type="button"
              >
                {renderItem(d, isSelectedEval)}
              </button>
            );
          }) :
            endOfList || <EndOfList />
          }
        </div>
        {renderFooter &&
          <div
            ref={footerRef}
            className={css.footer}
            id={`typedown-footer-${selectorSafe(input.name)}`}
          >
            {renderFooter(displayData, currentlyTyped, exactMatch)}
          </div>
        }
      </div>
    );
  }, [
    currentlyTyped,
    displayData,
    endOfList,
    exactMatch,
    footerRef,
    handleChange,
    handleNextFocus,
    input.name,
    input.value,
    isSelected,
    listKeyDownHandler,
    listRef,
    renderFooter,
    renderItem,
    searchWidth,
    selectedStyles,
    uniqueIdentificationPath
  ]);

  const renderSearchField = () => {
    return (
      <div
        ref={triggerRef}
        id={`typedown-parent-${selectorSafe(input.name)}-searchField`}
      >
        <SearchField
          // Pass meta through so correct styling gets applied to the TextField
          id={`typedown-searchField-${selectorSafe(input.name)}`}
          label={label}
          marginBottom0
          meta={meta}
          onChange={handleType}
          onKeyDown={searchFieldKeyDownHandler}
          required={required}
        />
      </div>
    );
  };

  const displayValue = useMemo(() => {
    return !!selectedUniqueId && (!open || displayValueWhileOpen);
  }, [displayValueWhileOpen, open, selectedUniqueId]);

  return (
    <div
      ref={resizeRef}
      className={classnames(
        css.typedown,
        className
      )}
      id={`typedown-id-${id}`}
    >
      {renderSearchField()}
      <Popper
        key="typedown-menu-toggle"
        anchorRef={triggerRef}
        className={classnames(
          css.dropdown,
          css.fullWidth
        )}
        isOpen={open}
        modifiers={{
          flip: { boundariesElement: 'viewport', padding: 10 },
          preventOverflow: { boundariesElement: 'viewport', padding: 10 }
        }}
        overlayProps={{
          'ref': overlayRef,
          'tabIndex': '-1',
          'onClick': (e) => { e.stopPropagation(); }  // prevent propagation of click events
        }}
        overlayRef={overlayRef}
        portal={portal}
      >
        {dropDown()}
      </Popper>
      {displayValue &&
        <div
          className={classnames(
            css.selectedDisplay
          )}
        >
          <div
            className={css.selectedItem}
          >
            {renderItem(input.value)}
          </div>
          {displayClearItem &&
            <IconButton
              className={css.clearItem}
              icon="times-circle-solid"
              onClick={() => handleChange()}
            />
          }
        </div>
      }
    </div>
  );
};

Typedown.propTypes = {
  className: PropTypes.string,
  dataOptions: PropTypes.arrayOf(PropTypes.object),
  displayClearItem: PropTypes.bool,
  displayValueWhileOpen: PropTypes.bool,
  endOfList: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.node,
    PropTypes.element
  ]),
  filterPath: PropTypes.string,
  id: PropTypes.string,
  initialOpenDelay: PropTypes.number,
  input: PropTypes.object,
  isSelected: PropTypes.func,
  label: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element
  ]),
  meta: PropTypes.object,
  onChange: PropTypes.func,
  onType: PropTypes.func,
  renderFooter: PropTypes.func,
  renderListItem: PropTypes.func,
  required: PropTypes.bool,
  selectedStyles: PropTypes.string,
  uniqueIdentificationPath: PropTypes.string
};

export default Typedown;
