## FormModal Component

The `FormModal` component provides a simple way to create a modal dialog with a form. It combines the `Modal` component from `@folio/stripes/components` with the `Form` component from `react-final-form`, handling form submission, validation, and modal interactions.

### Basic Usage

```javascript
import { useState } from 'react';
import FormModal from '@k-int/stripes-kint-components';
import { Field } from 'react-final-form';
import { TextField } from '@folio/stripes/components';

const MyFormModal = () => {
  const [open, setOpen] = useState(false);

  const onSubmit = (values) => {
    // Handle form submission
    console.log(values);
    setOpen(false); // Close the modal after submission
  };

  return (
    <>
      <Button onClick={() => setOpen(true)}>Open Modal</Button> {/* Button to open the modal */}
      <FormModal
        modalProps={{ open, onClose: () => setOpen(false) }}
        onSubmit={onSubmit}
      >
        <Field
          name="name"
          component={TextField}
          label="Name"
        />
      </FormModal>
    </>
  );
};
```

### Props

| Name | Type | Description | Default | Required |
|---|---|---|---|---|
| children | node/func | The content of the modal, typically form fields. |  | ✓ |
| intlKey | string | The key to use for internationalization messages. |  | ✕ |
| intlNS | string | The namespace to use for internationalization messages. |  | ✕ |
| labelOverrides | object | An object to override the default labels for buttons. | `{}` | ✕ |
| modalProps | object | Props to pass to the underlying `Modal` component.  Should include `open` and `onClose`. |  | ✓ |
| onError | func | A function to handle errors that occur during form submission.  This function will receive the error object as a parameter. |  | ✕ |
| onSubmit | func | A function to handle form submission. |  | ✓ |
| onSuccess | func | A function to handle successful form submission. This function will receive the submitted values as a parameter. |  | ✕ |
|...formProps | object | Any other props will be passed to the underlying `Form` component from `react-final-form`. |  | ✕ |

### Features

*   **Integrated form handling:** The component integrates seamlessly with `react-final-form`, providing access to form state and helper functions within the `footer` and `children` props.
*   **Customizable footer:** The `modalProps.footer` prop allows for customization of the modal footer. It receives an object with the following properties:
    *   `formState`: The current state of the form.
    *   `handleSubmit`: A function to handle form submission and close the modal.
    *   `handleClose`: A function to close the modal.
    *   `handleSubmitNoRestart`: **(DEPRECATED)** Use `handleSubmitRaw` instead.
    *   `handleSubmitRaw`: A function to handle form submission without closing the modal or clearing the form.
*   **Error and success handling:** The `onError` and `onSuccess` props allow for custom handling of asynchronous form submission errors and successes.
*   **Internationalization:** The component supports internationalization using the `intlKey` and `intlNS` props.
*   **Label overrides:** The `labelOverrides` prop allows for overriding the default button labels.

### Example with Custom Footer

```javascript
<FormModal
  modalProps={{
    open: true,
    onClose: () => { /*... */ },
    footer: ({ handleSubmit, handleClose }) => (
      <ModalFooter>
        <Button
          buttonStyle="primary"
          onClick={handleSubmit}
          type="submit"
        >
          Save
        </Button>
        <Button onClick={handleClose}>Cancel</Button>
      </ModalFooter>
    )
  }}
  onSubmit={() => { /*... */ }}
>
  {/*... form content... */}
</FormModal>
```