import PropTypes from 'prop-types';

import { Form } from 'react-final-form';
import { Button, Modal, ModalFooter } from '@folio/stripes/components';
import { useKintIntl } from '../hooks';

const FormModal = ({
  children,
  intlKey: passedIntlKey,
  intlNS: passedIntlNS,
  labelOverrides = {},
  modalProps: { footer, onClose, ...modalProps },
  onError, // Optional handler to run onSuccess for default handleSaveAndClear
  onSubmit,
  onSuccess, // Optional handler to run onSuccess for default handleSaveAndClear
  ...formProps
}) => {
  const kintIntl = useKintIntl(passedIntlKey, passedIntlNS);

  return (
    <Form
      onSubmit={onSubmit}
      {...formProps}
    >
      {({ handleSubmit, form: { getState, restart } }) => {
        const formState = getState();
        const handleClose = (e) => {
          onClose(e);
          restart();
        };

        // Handle asynchronous submit functions differently to synchronous ones
        const handleSaveAndClear = (...onSaveProps) => {
          try {
            const submitReturn = handleSubmit(...onSaveProps);

            // Figure out if we're in an async or sync submit function
            if (typeof submitReturn === 'object' && typeof submitReturn.then === 'function') {
              // Async function
              return submitReturn.then((incomingParams) => {
                restart();
                return incomingParams;
              }).then((incomingParams) => {
                if (onSuccess && typeof onSuccess === 'function') {
                  // Allow onSuccess to dictate what continues downstream
                  return onSuccess(incomingParams);
                }
                return incomingParams;
              }).catch((incomingParams) => {
                if (onError && typeof onError === 'function') {
                  // Allow onError to dictate what continues downstream
                  return onError(incomingParams);
                }
                return incomingParams;
              });
            } else if (onSuccess && typeof onSuccess === 'function') {
              // Sync function and we have an onSuccess handler

              // onSuccess dictates return
              const returnShape = onSuccess(submitReturn);
              restart();
              return returnShape;
            } else {
              // Sync function and we have no onSuccess handler

              restart();
              return submitReturn;
            }
          } catch (err) {
            // Sync function catch
            if (onError && typeof onError === 'function') {
              // Allow onError to dictate what continues downstream
              return onError(err);
            }

            return null;
          }
        };

        const renderFooter = () => {
          if (footer) {
            return footer({
              formState,
              handleSubmit: handleSaveAndClear,
              handleClose,
              handleSubmitNoRestart: handleSubmit, // DEPRECATED -- should use handleSubmitRaw
              handleSubmitRaw: handleSubmit
            });
          }

          const { invalid, pristine, submitting, validating } = formState;
          return (
            <ModalFooter>
              <Button
                buttonStyle="primary"
                disabled={submitting || invalid || pristine || validating}
                marginBottom0
                onClick={handleSaveAndClear}
                type="submit"
              >
                {kintIntl.formatKintMessage({
                  id: 'saveAndClose',
                  overrideValue: labelOverrides.saveAndClose
                })}
              </Button>
              <Button
                marginBottom0
                onClick={handleClose}
              >
                {kintIntl.formatKintMessage({
                  id: 'cancel',
                  overrideValue: labelOverrides.cancel
                })}
              </Button>
            </ModalFooter>
          );
        };

        return (
          <Modal
            enforceFocus={false}
            footer={renderFooter()}
            onClose={handleClose}
            {...modalProps}
          >
            {children}
          </Modal>
        );
      }}
    </Form>
  );
};

FormModal.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
    PropTypes.func,
  ]),
  intlKey: PropTypes.string,
  intlNS: PropTypes.string,
  labelOverrides: PropTypes.object,
  modalProps: PropTypes.shape({
    footer: PropTypes.func,
    onClose: PropTypes.func,
  }),
  onError: PropTypes.func,
  onSubmit: PropTypes.func,
  onSuccess: PropTypes.func
};

export default FormModal;
