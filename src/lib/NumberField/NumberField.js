import PropTypes from 'prop-types';
import {
  useEffect,
  useRef,
  useState
} from 'react';

import noop from 'lodash/noop';

import {
  nativeChangeFieldValue as nativeChangeField,
  TextField
} from '@folio/stripes/components';


const NumberField = (props) => {
  const {
    onBlur: passedOnBlur = noop,
    onChange: passedOnChange = noop,
    input,
    value,
    ...rest
  } = props;

  // Treat TextField ALWAYS as a controlled component, whether or not NumberField itself is controlled
  // This enforces the behaviour whereby 1e2 -> 100 in the typed field automatically
  const [forceControl, setForceControl] = useState(value ?? input?.value);

  const inputRef = useRef();
  const userInputRef = useRef();

  const [numValue, setNumValue] = useState('');

  const changeField = (val) => {
    nativeChangeField(inputRef, false, val);
  };

  // Allow direct control of field
  useEffect(() => {
    const valueToUse = value ?? input.value;

    if (!valueToUse && numValue) {
      // Make sure to empty out if it's cleared. Treating '' as empty instead of undefined
      setNumValue('');
    }

    if (valueToUse && numValue !== valueToUse) {
      setNumValue(valueToUse);
    }

    if (forceControl !== numValue) {
      setForceControl(numValue);
    }
  }, [forceControl, numValue, value, input]);

  const handleChange = (e) => {
    // Actually set the value in the form
    if (input?.onChange) {
      input.onChange(e);
    }

    // Set the forced control (this may be wiped by passedOnChange)
    setForceControl(e.target.value);

    // If the user has set up an onChange, this will ensure that that fires
    if (passedOnChange) {
      passedOnChange(e, e.target.value);
    }
  };

  const handleUserChange = (e) => {
    const parsedValue = parseFloat(e.target.value);

    // ReturnValue needed for controlled components
    if (parsedValue || parsedValue === 0) {
      setNumValue(parsedValue);
      changeField(parsedValue);
    } else {
      setNumValue('');
      changeField('');
    }
  };

  return (
    <>
      <TextField
        ref={userInputRef}
        {...rest} // Keep an eye on this
        onBlur={(event) => {
          // Make sure blur propogates to input
          if (input.onBlur) {
            input.onBlur(event);
          }
          passedOnBlur(event);
        }}
        onChange={handleUserChange}
        type="number"
        value={forceControl}
      />
      <input
        ref={inputRef}
        {...input}
        hidden
        id={input?.name}
        onChange={handleChange}
        type="number"
        value={numValue}
      />
    </>
  );
};

NumberField.propTypes = {
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  input: PropTypes.shape({
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    onBlur: PropTypes.func,
    value: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ])
  }),
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ])
};

export default NumberField;
