import { useState } from 'react';
import { Field, useForm } from 'react-final-form';

import userEvent from '@folio/jest-config-stripes/testing-library/user-event';
import { screen, waitFor } from '@folio/jest-config-stripes/testing-library/react';

import { Button, TestForm } from '@folio/stripes-erm-testing';
import { Button as StripesButton } from '@folio/stripes/components';

import NumberField from './NumberField';

import { renderWithKintHarness } from '../../../test/jest/helpers';


const onSubmit = jest.fn();

const NUMBER_FIELD_LABEL = 'TEST NUMBER FIELD';
const NUMBER_FIELD_ID = 'number-field-test';
const NUMBER_FIELD_INPUT_ID = 'input-id-test';

const RESET_BUTTON_LABEL = 'RESET NUMBER FIELD';

// We need to use jest selectors instead of interactors as the TextField interactor
// can't handle the double-text-field nature of NumberField
const getTextField = () => {
  // Spinbutton because it's a "number" type textField...
  return screen.getByRole('spinbutton', { name: NUMBER_FIELD_LABEL });
};

const testSubmitValues = (expectedValues) => {
  describe('submitting the form', () => {
    beforeEach(async () => {
      await waitFor(async () => {
        await Button('Submit').click();
      });
    });

    it('submits with expected values', async () => {
      await waitFor(() => {
        expect(onSubmit.mock.calls?.[0]?.[0]).toEqual(expectedValues);
      });
    });
  });
};

const SpecialResetButton = () => {
  const { change } = useForm();

  return (
    <StripesButton
      onClick={() => change(NUMBER_FIELD_INPUT_ID, undefined)}
    >
      {RESET_BUTTON_LABEL}
    </StripesButton>
  );
};

const NonControlledComponent = () => {
  return (
    <>
      <SpecialResetButton />
      <Field
        component={NumberField}
        id={NUMBER_FIELD_ID}
        label={NUMBER_FIELD_LABEL}
        name={NUMBER_FIELD_INPUT_ID}
      />
    </>
  );
};

const ControlledComponent = () => {
  const [value, setValue] = useState();

  return (
    <>
      <SpecialResetButton />
      <Field
        component={NumberField}
        id={NUMBER_FIELD_ID}
        label={NUMBER_FIELD_LABEL}
        name={NUMBER_FIELD_INPUT_ID}
        onChange={e => setValue(e.target.value)}
        value={value}
      />
    </>
  );
};

// EXAMPLE nesting repeated tests with describe.each or test.each might speed up some of our test writing
describe.each([
  ['Non-Controlled', <NonControlledComponent />],
  ['Controlled', <ControlledComponent />],
])('NumberField', (controlType, component) => {
  let _renderComponent;
  let textField;
  describe(`Regular usage (${controlType})`, () => {
    beforeEach(async () => {
      onSubmit.mockClear();
      _renderComponent = renderWithKintHarness(
        <TestForm
          initialValues={{}}
          onSubmit={onSubmit}
        >
          {component}
        </TestForm>
      );

      textField = getTextField();
    });

    it('renders text field as expected', () => {
      expect(textField).toBeInTheDocument();
    });

    describe.each([
      ['non-numeric characters', 'sdhukjasklfs', '', {}],
      ['numeric characters', '12345', '12345', { [NUMBER_FIELD_INPUT_ID]: '12345' }],
      ['scientific notation', '1e5', '100000', { [NUMBER_FIELD_INPUT_ID]: '100000' }],
      ['negative numbers', '-100', '-100', { [NUMBER_FIELD_INPUT_ID]: '-100' }],
    ])('Typing', (characterType, typedChars, expectedDisplay, expectedSubmit) => {
      describe(`typing ${characterType}`, () => {
        beforeEach(async () => {
          await waitFor(async () => {
            await userEvent.type(textField, typedChars);
          });
        });

        it('does not render typed characters', async () => {
          expect(textField).toHaveDisplayValue(expectedDisplay);
        });

        testSubmitValues(expectedSubmit);
      });
    });
  });

  describe.each([
    ['numeric initialvalues', { [NUMBER_FIELD_INPUT_ID]: '7654321' }, '7654321', { [NUMBER_FIELD_INPUT_ID]: '7654321' }],
    ['regular scientific initialvalue NOTE: scientific value parsing does NOT work from initialValues', { [NUMBER_FIELD_INPUT_ID]: '3e7' }, '3e7', { [NUMBER_FIELD_INPUT_ID]: '3e7' }],
    ['negative initialvalue', { [NUMBER_FIELD_INPUT_ID]: '-100' }, '-100', { [NUMBER_FIELD_INPUT_ID]: '-100' }],
  ])(`Initial values (${controlType})`, (describeTitle, initialValues, displayValue, expectedSubmit) => {
    describe(describeTitle, () => {
      beforeEach(async () => {
        onSubmit.mockClear();
        _renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={initialValues}
            onSubmit={onSubmit}
          >
            {component}
          </TestForm>
        );

        textField = getTextField();
      });

      it('renders initial value', async () => {
        expect(textField).toHaveDisplayValue(displayValue);
      });

      testSubmitValues(expectedSubmit);
    });
  });

  describe.each([
    [
      'Clear after typing',
      async () => {
        onSubmit.mockClear();
        _renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{}}
            onSubmit={onSubmit}
          >
            {component}
          </TestForm>
        );
        textField = getTextField();

        await waitFor(async () => {
          await userEvent.type(textField, '32786843');
        });
      }
    ],
    [
      'Clear from initialValues',
      async () => {
        onSubmit.mockClear();
        _renderComponent = renderWithKintHarness(
          <TestForm
            initialValues={{ [NUMBER_FIELD_INPUT_ID]: '32786843' }}
            onSubmit={onSubmit}
          >
            {component}
          </TestForm>
        );
        textField = getTextField();
      }
    ]
  ])(`ERM-3391: state change underneath component (${controlType})`, (clearType, beforeEachFunc) => {
    describe(clearType, () => {
      beforeEach(beforeEachFunc);

      it('renders text field', () => {
        expect(textField).toBeInTheDocument();
      });

      it('renders special reset button', async () => {
        await Button(RESET_BUTTON_LABEL).exists();
      });

      describe.each([
        [
          'Control',
          false,
          '32786843',
          { [NUMBER_FIELD_INPUT_ID]: '32786843' }
        ],
        [
          'After reset',
          true,
          '',
          {}
        ]
      ])('Test reset', (describeTitle, clearField, expectedValue, expectedSubmit) => {
        describe(describeTitle, () => {
          beforeEach(async () => {
            onSubmit.mockClear();
            if (clearField) {
              await waitFor(async () => {
                await Button(RESET_BUTTON_LABEL).click();
              });
            }
          });

          it(`renders text field with display value: ${expectedValue}`, async () => {
            await waitFor(() => {
              expect(textField).toHaveDisplayValue(expectedValue);
            });
          });

          testSubmitValues(expectedSubmit);
        });
      });
    });
  });
});
