const babelJest = require('babel-jest');
const babelConf = require('../../.babelrc');

module.exports = babelJest.createTransformer(babelConf);
