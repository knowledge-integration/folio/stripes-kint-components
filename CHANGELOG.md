# [5.2.0](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/compare/v5.1.0...v5.2.0) (2023-10-23)


### Bug Fixes

* add semantic release script to package.json ([834206b](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/commit/834206b41d67e9931ad9d02dc706d35f37b816c7))
* Sorting ([6c7d321](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/commit/6c7d321a92cca6209be114cc2495b346d9c6c7ee))


### Features

* Added comparator ([e955e4e](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/commit/e955e4ee593208b1ae2a30f5603e9fb3d1469a67))
* **build:** Cause a bump to 5.2 and update reade ([582333d](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/commit/582333d46fcd489ea01a820199fbffeef7ce7280))
* CustomPropertyField multi-value ([1fb183d](https://gitlab.com/knowledge-integration/folio/stripes-kint-components/commit/1fb183d9792947a6180a4a9d013ac33900d89dad))

## 5.2.0 In progress
  * comparators - added !~ (Does not contain) comparator to constants array
  * CustomPropertyEdit
    * Added sorting by label to refdata type custom property options
## 5.1.0 2023-09-26
  * KIWT filter parsers
    * parseKiwtQueryFilters - will parse grouped KIWT query strings into structured array
    * parseKiwtQueryFiltersRecursive - will do the same as the above, after it has already run through parseKiwtQueryGroups
    * deparseKiwtQueryFilters - will take an array of the shape created by parseKiwtQueryFilters and return it to string form
    * deparseKiwtQueryFiltersObject - will take a singular object of the shape specified by the array created in parseKiwtQueryFilters, and will return a string for that singular object.
  * generateKiwtQueryParams - some null safety
  * buildUrl - small refactor
  * dependency bumps
  * test tweaks

## 5.0.0 2023-08-07
  * Fixed rogue CSS from ComboButton
  * Removed typo deprecated export customPropertyContants
  * Bumped dependencies to bring in line with react18 version of stripes
## 4.7.0 2023-06-06
  * Added success callout to SettingsFormContainer
  * Bumped @rehooks/local-storage dep to ^2.4.4
  * Added indexing to ActionList parent div for uniqueness for screen readers
  * Added aria-label to default Fields within ActionList
  * ComboButton - new component for buttonm with one primary and multiple ancillary actions.

## 4.6.0 2023-04-14
  * ERM-2884 Regex with lookbehind causes render failure in Safari
  * Deprecated customPropertyContants typo. For now accessible both at customPropertyContants and customPropertyConstants, will be removed in future version.
  * endpoints -- new constants available from kint-components
  * useModConfigEntries -- new hook to link up to mod-config
  * useMutateModConfigEntries -- hook to mutate same resources as above
  * modConfigEntriesueryKey -- utility function to get a hold of the query key used internally
  * Test tweaks so they should run externally from workspace (After release of @folio/stripes-erm-testing 1.2.0)


## 4.5.0 2023-03-16
  * ERM-2879 FOLIO > Agreement App > Supplementary properties is showing the value of 'Visibility' as 'Public' regardless of selection
  * selectorSafe -- Ensured better compatibility with other scripts now it's used in a user facing place.
  * useHelperApp: No longer redraws when keys of passed helper objects have not changed
  * Export all hooks by default, both regular hooks and settings specific hooks
  * QueryTypedown
    * Added 'userQuery' state and a useEffect for the 'callPath' so that it is correctly updated upon a user input change
  * ResponsiveButtonGroup
    * Now fires an event 'responsive-button-group-loaded' or 'responsive-button-group-loaded-<id>' depending on whether or not an id has been passed to the ResponsiveButtonGroup
## 4.4.1 2023-02-08
  * CustomPropertiesSettings
    * Added displayConditions which act in the same way as below, allowing for programatic turning on/off of access to certain actions
  * Settings (Multiple components)
    * Added allowEdit property, which defaults to `true`, and if false, does not render edit button for any appSetting
    * Added allowGlobalEdit property to `useSettings` hook, which allows the programatic turning on/off of `allowEdit` for ALL settings. More granular control will currently have to be handled manually via `dynamicPageExclusions` and then manual setting up of SettingsPage components with the requisite `allowEdit` booleans controlled by the implementing software.
## 4.4.0 2023-02-08
  * Removed all cyclic imports, linting
  * ActionList
    * Added hideActionsColumn prop, if true it will hide the actions column, treating the actionList instead as an MCL. (The column will still show while editing/creating, in case this is triggered externally, so as to avoid soft locking.)
  * EditableRefdataList
    * Added displayConditions property, which will be used to additionally deny actions to the user. Options `{create: true, delete:true, edit:true}` are passed by default, but each can be turned off to hide a given action.
  * EditableRefdataList
    * Added displayConditions property, which will be used to additionally deny actions to the user. Options `{create: true, delete:true}` are passed by default, but each can be turned off to hide a given action.
    * In addition to the displayConditions above, the Actions column will now hide automatically if no RefdataCategories exist with 0 values (And therefore are deletable)
  * Typedown
    * More styling changes
    * Added displayClearItem prop, defaults to `true`. When `false` the clear item button will not render.

## 4.3.0 2023-01-27
  * ActionList
    * Tweaks to props passed to fieldComponents functions
    * Same property object passed to validateFields now
  * EditableRefdataList
    * Prop added `allowSpecial`, default value `false`
      * With this property set to false, the component on creation will strip out all non-alphanumeric and non dash/underscore characters, and replace them with `_` for the _value_ field, it will not affect the _label_ of the refdata (Since the value field is created from the entered label).
  * ResponsiveButtonGroup
    * Added dropdownTriggerProps to pass extra implementation props to the dropdown menu trigger
    * Added default aria-label="menu" to the dropdownTriggerProps
  * SASQLookupComponent
    * Tweaked filterPaneProps and mainPaneProps, now lastMenu and firstMenu will get access to certain internal state, such as filterCount, filterPaneVisible (and handlers), among others.
    * Added tooltip to collapse/expand filter pane as default. (Swapped internally to use stripes-smart-components to bring in line with other apps)
  * Typedown
    * Small styling changes, rounded corners of the dropdown menu now depend on whether the menu is rendered below, above, or floating

  * Avoid calling setState after unmounting in `useReponsiveButtonGroupSizing`, which can happen due to `debounce`.

## 4.2.0 2022-12-09
  * Changed all test resources over to stripes-erm-testing and jest manual mocks
  * ResponsiveButtonGroup
    * Styling tweaks
    * Added marginBottom0 prop
  * Added parseBracketGroup utility function
  * Changed export of utilities to wildcard * in main index.js
  * Added groupValues option to generateKiwtQueryParams, allowing for bracketed nesting of filter boolean logic
  * ActionList stop propagation of action click events, to improve behaviour when a rowClick is defined
  * parseErrorResponse now includes a status code, where applicable
## 4.1.0 2022-11-10
  * generateKiwtQuery now encodes each interior query chunk using `encodeURIComponent` instead of using `encodeURI` on the entire chunk. ie `filters=(checklist.definition.name%3D%3Dtest_1%26%26checklist.outcome.value%3D%3Dyes)` (Everything after `filters=` is encoded).
## 3.2.0 2022-11-10
  * RichSelect now passes `disabled` prop to Dropdown, so trigger will no longer be overriden by `disabled: undefined` from `getTriggerProps`.

## 3.1.0 2022-11-03
  * Added `searchFieldAria label` and `searchFieldProps` to SASQLookupComponent 
  * Removed unnecessary validate prop on CustomPropertyField, fixed broken out of range validator
  * Can now pass ref to SASQRoute, SASQLookupComponent and SASQViewComponent to obtain query props
  * Added hook useSASQQueryMeta, to give query namespaces and invalidation functions
  * Fixed inconsistency between CustomPropertiesEditCtx and CustomPropertiesViewCtx labelOverrides
  * Fixed issue where custom property ctx is not removable
  * Fixed bug in intlKeyStore where multiple keys being added would break the structure
  * PropTypes tweak of NoResultsMessage
  * ResponsiveButtonGroup
  * Added persistedPanesetProps prop to SASQRoute (etc) to enable paneset prop wrangling

## 3.0.0 2022-09-16
  * RichSelect default trigger now uses DropdownButton under the hood -- changed styling
  * ActionList previously added actionListItems directly to cached object--sometimes this caused the react-query cache ITSELF to update, although I'm not sure why here and not anywhere else. Fixed by creating a clone internally in ActionListFieldArray and assigning actionListActions on that.
  * New util function `parseErrorResponse`, used in places where we catch http errors, allows for text only response bodies
  * onRowClick supplied to ActionList now controls whether or not the underlying MCL is considered "interactive"
  * ActionList actions now accept `tooltip` and `tooltipSub` props to control a wrapper Tooltip on the action
  * Complete overhaul of translations and labelOverrides. Not backwards compatible, hence major version bump.
    * Translations now require a setup hook "useIntlKeyStore" to set up a favoured intl key, then use that throughout:

    ```
    const addKey = useIntlKeyStore(state => state.addKey);
    addKey('ui-agreements');
    ```

    * Translations can be overridden by labelOverrides, but these must now ALWAYS be a String, either direct translated string value, or an intl key that can then be passed certain values where applicable
  * Removed deprecated "actionCalls" prop from ActionList
  * Removed `useAvailableCustomProperties`

## 2.8.0 2022-07-15
  * Fixed bug where non-createable fields which were editable would display a field on creation.
  * RichSelect -- A Select style form component allowing for more custom render behaviour.
    * Refactored IconSelect to use RichSelect under the hood
  * EditableRefdataList/EditableRefdataCategoryList Added validation to prevent attempting null save
    * PropTypes updated
    * ActionList Save button is now disabled if the form has submissionErrors.
  * ActionList Added a prop to bootstrap defaults in while adding new object inline.
  * Linting and refactoring

## 2.7.0 2022-07-05
  * Fixed missing translation issue
  * EditableRefdataCategoryList component set up for ease of refdata category creation/deletion
    * Also exposed useMutateRefdataCategory that it uses under the hood
  * Custom property weight sorting not working as expected
  * Fix aria label null safety in ActionList
  * refdataOptions now sorts by desc by default
  * ActionList
    * autoFocus only applied to first field in editing row.
    * formatter accidentally applied over editing fields
    * validateFields prop to allow validation of fields. Takes an object with keys from `visibleFields` and values which are functions accepting the rowData, and returning a function to be used for validation in final-form.
  * CustomProperties added default `labelOverrides={}` to all components to allow proper modular use without unnecessary props.
  * CycleButton component
  * IconSelect component

## 2.6.0 2022-06-20
  * UIOA-118 App does not function when code-splitting is enabled - no longer set history when url has not changed
  * ERM-2137 Support multi-select refdata properties in Agreement/Licenses settings
  * ERM-2173 UX tweaks to custom property configuration and views
  * Added ConfirmationModal and error callout for deletion of refdata to EditableRefDataList
  * useAppSettings hook for easy fetching of an AppSetting
  * ActionList
    * New ActionList fields are now added to the start of the list
    * Buttons within the ActionList can now be passed specfic aria-labels
  * Query handling.
    * useMutateCustomProperties, useMutateRefdataValues, CustomPropertiesSettings and EditableRefdataList now all accept both `afterQueryCalls` and `catchQueryCalls` to allow implementors to trigger specific handling after queries/when queries throw.
    * The default implementation in both components above is now a standard deletion failure message: `"<strong>Error: {label}</strong> was not deleted. {error}"`. This can alternatively be overriden with a `labelOverrides` prop: `{ deleteError: (error, object) => { ... output a callout message } }`
  * Some peer deps changed to dev-deps
  * ERM-2144 Support multi-select refdata properties in License term filter builder

## 2.5.0 2022-05-19
  * Added FilterConfig support to generateKiwtQueryParams
  * Added a way to trigger inline creation externally through refs and useImperativeHandle
  * Fix for ActionList editing. When a formatter was in place and a field was not editable, the formatter was ignored.
  * Added disabled prop to create button so it is no longer to create infinite new rows, or enter create while editing another row.
  * "Show filters" button on SASQ main pane still displayed when filters are visible
  * AppSettings now use generateKiwtQueryParams and are not limitied to 10.
  * FormModal now disables default footer button when form is invalid/prisitn/submitting (Submit handler must be asyncronous for submitting to be parsed correctly)
  * Typedown labels are now accessible

## 2.4.0 2022-04-21
  * Custom Property component fixes
  * Fix to actionAssigner callback pattern
  * Tweak to remove marginBottom0 warning
  * FormModal buttons have marginBottom0 prop on them now
  * Fixed issue where cusotm property updates would not be reflected on the open view pane in settings
  * Fixed inconsistent `retiredName` labelOverride function behaviour, where in `filters` the function accepted the name, and in `view` the function accepted the entire object. The behaviour has been standardised to be the name alone in both scenarios.
  * Fixed improper custom property count behaviour in view mode, now only _set_ primary properties are included in the count.
  * ActionListFieldArray changed autofocus to autoFocus to avoid react console noise
  * Refactored SASQ Components to fix various bugs/oddities
    * TableBody default now exported as SASQTableBody from module
    * Table now no longer redraws on row click
    * Formatting tweaks to fix horizontal scroll issue
    * Manually clearing the search field now has the same effect as clicking the "x" button to clear, ensuring the items are refetched without a search term.
    * Implementor can pass props to the filter pane via "filterPaneProps" on SASQRoute/SASQLookupComponent
    * boldString, highlightString, matchString util functions

## 2.3.0 2022-03-25
  * FormModal
    * Ability to override labels for "save" and "cancel"
    * Ability to override whole footer section
  * CustomProperties Edit components:
    * CustomPropertiesEdit
    * CustomPropertiesEditCtx
    * CustomPropertiesListField
    * CustomPropertyField
    * CustomPropertyFormCard
    * useAvailableCustomProperties
  * Refdata hooks and utils
    * useInvalidateRefdata hook
    * refdataQueryKey utility function
    * useRefdata refactored to use the above
    * useMutateRefdata now invalidates query cache after changes
  * QueryTypedown
    * Added "dataFormatter" prop, to allow massaging of data between fetch and injections into Typedown
  * ActionList
    * ActionCalls deprecated in favour of a direct `callback` prop on actionAssigner, and separate createCallback prop.
    * `to` prop on actionAssigner allowing for rendering of <Link/> as opposed to <Button>/<IconButton/>
## 2.2.0 2022-03-11
* Fix -- GenerateQueryParams no longer assumes you want EITHER nsValues filters or options filters, it simply adds both to the params array
* ActionList -- added a hideCreateButton prop for custom rolled solutions
* FormModal -- No longer restart form on submit... should stop validation errors clearing the form
* Added support for Date type custom properties, as well as retired prop.
* Shift away from stripes-erm-components validators to internally defined validators.
## 2.1.0 2022-03-09
* Fixes for ActionList
  * When no formatter is present, it no longer crashes
  * Formatting for buttons
* Fixes for CustomProperty work
  * Editing CustProp with context no longer crashes
  * Added translation for "no matching context found"
  * Custom properties now sorted by label by default
* generateKiwtQuery now allows for filters and sort objects to be passed in with options, to avoid weird parsing nsValues used by SASQ if desired.
  * useCustomProperties migrated over to use this feature
  * SASQViewComponent now hands "queryProps" to the ViewComponent, allowing access to the react-query internals
  * SASQRoute children are now rendered above the SASQViewComponent, allowing for Route patterns such as 'Charges' in OA
  * FormModal is actually exported now
  * Typedown
    * Interaction styles have been improved, onHover and onFocus styles are now in line with Stripes standards
    * Can now pass className to Field containing Typedown and they will be applied to Typedown
    * Selected item style is no longer "selected" colour, instead blank with border

## 2.0.0 2022-03-04
  * ActionList updates
    * Formatter now accepted, will apply while not editing a row
    * MCL props not specified in prop list will be passed directly to MCL
    * fieldComponents prop allows for rendering of a custom Field for use in editing a specific field on a row
  * Removed deprecated TypeDown component (replaced by Typedown in v1.3.0)
  * Typedown
    * Added isSelected prop
    * Added id prop
    * Added label prop
    * Added required prop
    * Added endOfList prop 
  * FormModal component
  * Custom Property configuration components
    * CustomPropertiesLookup
    * CustomPropertiesView
    * CustomPropertiesSettings
  * Added jest artifacts one level below root to gitignore
## 1.7.0 Released 20th January 2022
 * EditableRefdataList fixes and sorting
 * Fixes to create action in actionlist
 * creatableFields added to ActionList
 * refdataOptions -- a default SASQ options setup is now exported
 * Added FilterPaneHeaderComponent to SASQLookupComponent
 
## 1.6.0 Released 10th December 2021
 * RefdataButtons now exposed
 * Jest testing framework now up and operational
   * As part of that, now contains dev-dependencies on:
     * react-router-dom
     * @folio/stripes
     * react-intl
     * regenerator-runtime
 * Multitude of linting fixes
 * Refactor of settings components
 * Refactor of typedown hooks
 * Typedown now uses focus functions from stripes-components rather than internal copy
 * Fixed typedown footer focus bug

## 1.5.0 Released 9th December 2021
 * useKiwtFieldArray hook

## 1.4.0 Released 26th Nov 2021
 * useHelperApp now exposes an `isOpen` function to check if current helper is open or not.
 * useQIndex hook exposed to allow setting and parsing of qindex from url. QIndex now used in useKiwtSASQuery.
 * useRefdata now accepts options, and defaults to 100 returns.
 * GenerateKiwtQueryParams now exposed as well as the full generateKiwtQuery
 * selectorSafe function exported
 * NoResultsMessage component, used in SASQLookupComponent

## 1.3.0 Released 9th Nov 2021
 * Added SASQRoute, SASQLookupComponent and SASQViewComponent
 * Deprecated TypeDown
 * Split TypeDown into two components, Typedown and QueryTypedown
 * Added resize-detector to dependency list
 * fix: typedowns can now open independently of each other
 * fix: meta now spread correctly onto SearchField in Typedown
 * feat: TypeDown now accepts an "onChange" handler (which takes the changed value instead of an event)
## 1.2.0 Released 22nd Oct 2021
 * generateKiwtQuery utility function
 * useHelperApp, useActiveElement and useKiwtSASQuery hooks
 * SearchField Form Component
 * TypeDown Form Component
 * Fixed issue where EditableRefdataList was sending actions along with row data for edit
 * Documentation
## 1.1.0 Released 16th Sept 2021
 * Fixed stripes dependency issues
 * Added Babel transpilation so module can run properly in FOLIO context
 * Changed deps to peer deps so as not to rely on one version of react/stripes dependencies
 * Introduced prop to get entire queryObject from useQuery in useRefdata rather than just the data.
 * New hook, useMutateRefdataValues, which provides simple access to put/delete operations for refdataValues
 * New component, ActionList which can render items and apply actions on them
 * New component, EditableRefdataList. Uses ActionList to render a refdata editor with just two props, desc and refdataEndpoint
 * useRefdata expanded to accept desc prop of type array as well as string.
## 1.0.0 Released 19th Aug 2021
  * Settings files migrated away from ui-rs
  * useTemplates hook created
  * useRefdata hook created
  * Set up npm publishing to the @k-int scope
